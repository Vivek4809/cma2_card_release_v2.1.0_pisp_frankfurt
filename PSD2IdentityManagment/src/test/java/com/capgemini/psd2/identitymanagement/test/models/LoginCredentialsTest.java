package com.capgemini.psd2.identitymanagement.test.models;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.capgemini.psd2.identitymanagement.models.LoginCredentials;

public class LoginCredentialsTest {

	private String username = "abcd"; 
	private String password="1234";
	private LoginCredentials loginCredentials;

	@Before
	public void setUp() throws Exception {
		loginCredentials = new LoginCredentials();
	}
	
	@Test
	public void testForGetUsername()
	{
		loginCredentials.setUsername(username);
		assertEquals(username, loginCredentials.getUsername());
		
	}
	@Test
	public void testForGetPassword()
	{
		loginCredentials.setPassword(password);
		assertEquals(password, loginCredentials.getPassword());
	}
	
	@After
	public void tearDown() throws Exception {
		loginCredentials = null;
	}
}
