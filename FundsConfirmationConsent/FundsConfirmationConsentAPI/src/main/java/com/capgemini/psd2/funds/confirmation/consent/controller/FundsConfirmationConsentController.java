package com.capgemini.psd2.funds.confirmation.consent.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsent1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.cisp.utilities.CommonCardValidations;
import com.capgemini.psd2.funds.confirmation.consent.service.FundsConfirmationConsentService;


@RestController
public class FundsConfirmationConsentController {
	
	@Autowired
	CommonCardValidations commonCardValidations;

	@Autowired
	private FundsConfirmationConsentService fundsConfirmationConsentService;



	@RequestMapping(value = "/funds-confirmation-consents", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public OBFundsConfirmationConsentResponse1 createFundsConfirmationConsent(
			@RequestBody OBFundsConfirmationConsent1 fundsConfirmationConsentPOSTRequest) {
    
      return fundsConfirmationConsentService.createFundsConfirmationConsent(fundsConfirmationConsentPOSTRequest);
	}

	@RequestMapping(value = "/funds-confirmation-consents/{ConsentId}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public OBFundsConfirmationConsentResponse1 getFundsConfirmationConsent(
			@PathVariable("ConsentId") String consentId) {
		
		return fundsConfirmationConsentService.getFundsConfirmationConsent(consentId);

	}

	@RequestMapping(value = { "/funds-confirmation-consents/{ConsentId}",
			"/internal/funds-confirmation-consents/{ConsentId}" }, method = RequestMethod.DELETE, produces = {
					MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteFundsConfirmationConsent(@PathVariable("ConsentId") String consentId) {

		fundsConfirmationConsentService.removeFundsConfirmationConsent(consentId);

	}
}