package com.capgemini.psd2.funds.confirmation.consent.routing.adapter.routing;

import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;

@FunctionalInterface
public interface FundsConfirmationConsentAdapterFactory {

	/**
	 * Gets the adapter instance.
	 *
	 * @param adapterName
	 *            the adapter name (i.e mongoDb/Client adpater)
	 * @return the adapter instance
	 */
	public FundsConfirmationConsentAdapter getAdapterInstance(String adapterName);

}
