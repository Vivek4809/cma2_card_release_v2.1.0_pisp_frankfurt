package com.capgemini.psd2.fraudsystem.adapter;

import java.util.Map;

public interface FraudSystemAdapter {

	public <T> T retrieveFraudScore(Map<String, Map<String, Object>> fraudSystemRequest);

}