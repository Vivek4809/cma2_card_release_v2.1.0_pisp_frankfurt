package com.capgemini.psd2.cisp.adapter;

import java.util.Map;

import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1;
import com.capgemini.psd2.consent.domain.AccountDetails;

public interface FundsConfirmationAdapter {

	public OBFundsConfirmationResponse1 getFundsData(AccountDetails accountDetails,
			OBFundsConfirmation1 fundsConfirmationPOSTRequest, Map<String, String> params);

}
