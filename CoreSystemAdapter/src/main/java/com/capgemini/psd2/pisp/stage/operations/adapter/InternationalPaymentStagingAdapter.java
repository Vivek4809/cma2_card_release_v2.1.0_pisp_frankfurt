package com.capgemini.psd2.pisp.stage.operations.adapter;

import java.util.Map;

import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public interface InternationalPaymentStagingAdapter {
	// Called from International Payment Consents API
	public CustomIPaymentConsentsPOSTResponse processInternationalPaymentConsents(
			CustomIPaymentConsentsPOSTRequest internationalPaymentConsentsRequest,
			CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,OBExternalConsentStatus1Code successStatus,OBExternalConsentStatus1Code failureStatus);

	// Called from International Payment Consents API, Submission API, Consent Application
	public CustomIPaymentConsentsPOSTResponse retrieveStagedInternationalPaymentConsents(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params);

}
