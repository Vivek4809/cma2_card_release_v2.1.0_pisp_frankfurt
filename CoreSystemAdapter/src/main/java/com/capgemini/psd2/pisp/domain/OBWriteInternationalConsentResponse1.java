package com.capgemini.psd2.pisp.domain;

import java.util.Objects;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsentResponse1;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * OBWriteInternationalConsentResponse1
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-09-05T13:53:27.174+05:30")

public class OBWriteInternationalConsentResponse1   {
  @JsonProperty("Data")
  private OBWriteDataInternationalConsentResponse1 data = null;

  @JsonProperty("Risk")
  private OBRisk1 risk = null;
  
  //Added manually
  @JsonProperty("Links")
  private PaymentSetupPOSTResponseLinks links = null;

  //Added manually
  @JsonProperty("Meta")
  private PaymentSetupPOSTResponseMeta meta = null;

public OBWriteInternationalConsentResponse1 data(OBWriteDataInternationalConsentResponse1 data) {
    this.data = data;
    return this;
  }

  /**
   * Get data
   * @return data
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public OBWriteDataInternationalConsentResponse1 getData() {
    return data;
  }

  public void setData(OBWriteDataInternationalConsentResponse1 data) {
    this.data = data;
  }

  public OBWriteInternationalConsentResponse1 risk(OBRisk1 risk) {
    this.risk = risk;
    return this;
  }
  
  public OBWriteInternationalConsentResponse1 links(PaymentSetupPOSTResponseLinks links) {
	this.links = links;
	return this;
  }
  
  public OBWriteInternationalConsentResponse1 meta(PaymentSetupPOSTResponseMeta meta) {
	this.meta = meta;
	 return this;
  }

  /**
   * Get risk
   * @return risk
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public OBRisk1 getRisk() {
    return risk;
  }

  public void setRisk(OBRisk1 risk) {
    this.risk = risk;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBWriteInternationalConsentResponse1 obWriteInternationalConsentResponse1 = (OBWriteInternationalConsentResponse1) o;
    return Objects.equals(this.data, obWriteInternationalConsentResponse1.data) &&
        Objects.equals(this.risk, obWriteInternationalConsentResponse1.risk);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data, risk);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBWriteInternationalConsentResponse1 {\n");
    
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("    risk: ").append(toIndentedString(risk)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
  public PaymentSetupPOSTResponseLinks getLinks() {
		return links;
	}

	public void setLinks(PaymentSetupPOSTResponseLinks links) {
		this.links = links;
	}

	public PaymentSetupPOSTResponseMeta getMeta() {
		return meta;
	}

	public void setMeta(PaymentSetupPOSTResponseMeta meta) {
		this.meta = meta;
	}
}

