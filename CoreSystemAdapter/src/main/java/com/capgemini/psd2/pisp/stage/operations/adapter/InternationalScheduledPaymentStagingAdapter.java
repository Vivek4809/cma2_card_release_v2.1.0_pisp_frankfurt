package com.capgemini.psd2.pisp.stage.operations.adapter;

import java.util.Map;

import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentConsentResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public interface InternationalScheduledPaymentStagingAdapter {
	//Called from Domestic Consents API
	public GenericPaymentConsentResponse processInternationalScheduledPaymentConsents(CustomISPConsentsPOSTRequest internationalScheduledPaymentRequest, CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,OBExternalConsentStatus1Code successStatus,OBExternalConsentStatus1Code failureStatus);
	
	//Called from Domestic Consents API, Submission API, Consent Application
	public CustomISPConsentsPOSTResponse retrieveStagedInternationalScheduledPaymentConsents(CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params);		 
	
}
