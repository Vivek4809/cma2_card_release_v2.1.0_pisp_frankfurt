package com.capgemini.psd2.pisp.adapter;

import java.util.Map;

import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentFileSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PlatformFilePaymentsFileResponse;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public interface FilePaymentsAdapter {

	public PlatformFilePaymentsFileResponse downloadFilePaymentsReport(String paymentId);

	public CustomFilePaymentsPOSTResponse findFilePaymentsResourceStatus(CustomPaymentStageIdentifiers stageIdentifiers);

	public PaymentFileSubmitPOST201Response processPayments(CustomFilePaymentsPOSTRequest paymentConsentsRequest,
			Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params);

	public PaymentFileSubmitPOST201Response retrieveStagedFilePayments(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params);

}
