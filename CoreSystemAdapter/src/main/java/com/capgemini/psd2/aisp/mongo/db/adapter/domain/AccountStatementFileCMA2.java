package com.capgemini.psd2.aisp.mongo.db.adapter.domain;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="MockAccountStatementFileCMA2")
public class AccountStatementFileCMA2 {
	
	private String statementId;

	private String statementFileName;
	
	private String statementUrl;

	public String getStatementId() {
		return statementId;
	}

	public void setStatementId(String statementId) {
		this.statementId = statementId;
	}

	public String getStatementFileName() {
		return statementFileName;
	}

	public void setStatementFileName(String statementFileName) {
		this.statementFileName = statementFileName;
	}

	public String getStatementUrl() {
		return statementUrl;
	}

	public void setStatementUrl(String statementUrl) {
		this.statementUrl = statementUrl;
	}
	
	
}
