package com.capgemini.psd2.aisp.adapter;

import java.util.Map;

import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSchedulePaymentsResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

public interface AccountSchedulePaymentsAdapter {

	public PlatformAccountSchedulePaymentsResponse retrieveAccountSchedulePayments(AccountMapping accountMapping, Map<String, String> params);
}
