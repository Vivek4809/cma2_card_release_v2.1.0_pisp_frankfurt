package com.capgemini.psd2.aisp.account.identfication.mapping.adapter.domain;

import java.util.Map;

import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

@Validated
public class IdntificationMapping {
	private String identifier;
	private String accountNumber;
	private String accountNSC;
	private String iban;
	private String bicfi;
	private String pan;
	private Map<String, String> additionalInfo;

	@NotNull
	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	@NotNull
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@NotNull
	public String getAccountNSC() {
		return accountNSC;
	}

	public void setAccountNSC(String accountNSC) {
		this.accountNSC = accountNSC;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getBicfi() {
		return bicfi;
	}

	public void setBicfi(String bicfi) {
		this.bicfi = bicfi;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public Map<String, String> getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(Map<String, String> additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
}
