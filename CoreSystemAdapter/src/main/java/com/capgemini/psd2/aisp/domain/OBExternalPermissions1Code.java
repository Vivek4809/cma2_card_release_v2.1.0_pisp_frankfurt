package com.capgemini.psd2.aisp.domain;

import java.util.Objects;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Specifies the Open Banking account access data types. This is a list of the data clusters being consented by the PSU, and requested for authorisation with the ASPSP.
 */
public enum OBExternalPermissions1Code {
  
  READACCOUNTSBASIC("ReadAccountsBasic"),
  
  READACCOUNTSDETAIL("ReadAccountsDetail"),
  
  READBALANCES("ReadBalances"),
  
  READBENEFICIARIESBASIC("ReadBeneficiariesBasic"),
  
  READBENEFICIARIESDETAIL("ReadBeneficiariesDetail"),
  
  READDIRECTDEBITS("ReadDirectDebits"),
  
  READOFFERS("ReadOffers"),
  
  READPAN("ReadPAN"),
  
  READPARTY("ReadParty"),
  
  READPARTYPSU("ReadPartyPSU"),
  
  READPRODUCTS("ReadProducts"),
  
  READSCHEDULEDPAYMENTSBASIC("ReadScheduledPaymentsBasic"),
  
  READSCHEDULEDPAYMENTSDETAIL("ReadScheduledPaymentsDetail"),
  
  READSTANDINGORDERSBASIC("ReadStandingOrdersBasic"),
  
  READSTANDINGORDERSDETAIL("ReadStandingOrdersDetail"),
  
  READSTATEMENTSBASIC("ReadStatementsBasic"),
  
  READSTATEMENTSDETAIL("ReadStatementsDetail"),
  
  READTRANSACTIONSBASIC("ReadTransactionsBasic"),
  
  READTRANSACTIONSCREDITS("ReadTransactionsCredits"),
  
  READTRANSACTIONSDEBITS("ReadTransactionsDebits"),
  
  READTRANSACTIONSDETAIL("ReadTransactionsDetail");

  private String value;

  OBExternalPermissions1Code(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static OBExternalPermissions1Code fromValue(String text) {
    for (OBExternalPermissions1Code b : OBExternalPermissions1Code.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

