package com.capgemini.psd2.revoke.consent.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.product.db.resources.locator.MultiTenancyRequestBean;
import com.capgemini.psd2.revoke.consent.routing.impl.RevokeConsentRoutingAdapter;
import com.capgemini.psd2.revoke.consent.service.RevokeConsentService;
import com.capgemini.psd2.revoke.consent.validator.RevokeConsentValidatorImpl;

@Service
public class RevokeConsentServiceImpl implements RevokeConsentService {

	@Autowired
	@Qualifier("revokeConsentRoutingAdapter")
	private RevokeConsentRoutingAdapter revokeConsentAdapter;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Value("${cmaVersion}")
	private String cmaVersion;

	@Autowired
	private RevokeConsentValidatorImpl revokeConsentValidator;
	
	@Autowired
	private MultiTenancyRequestBean multiTenantReqBean;

	@Override
	public void removeConsentRequest(String consentId, String tenantId) {
		multiTenantReqBean.setTenantId(tenantId);
		revokeConsentValidator.validateConsentId(consentId);
		revokeConsentAdapter.revokeConsentRequest(consentId, reqHeaderAtrributes.getTppCID());
	}

}
