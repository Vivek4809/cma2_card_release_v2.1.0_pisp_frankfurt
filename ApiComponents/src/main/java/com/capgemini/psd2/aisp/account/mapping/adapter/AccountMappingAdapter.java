package com.capgemini.psd2.aisp.account.mapping.adapter;

import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;

public interface AccountMappingAdapter {

	public AccountMapping retrieveAccountMappingDetails(String accountId, AispConsent aispConsent);

	public AccountMapping retrieveAccountMappingDetails(AispConsent aispConsent);
	
}
