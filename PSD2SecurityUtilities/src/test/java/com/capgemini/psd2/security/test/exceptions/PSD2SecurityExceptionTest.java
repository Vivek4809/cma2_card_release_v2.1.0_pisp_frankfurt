package com.capgemini.psd2.security.test.exceptions;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;


public class PSD2SecurityExceptionTest {

	private PSD2SecurityException psd2SecurityException;
	private ErrorInfo errorInfo ;
	private SCAConsentErrorCodeEnum errorCodeEnum;
	@Before
	public void setUp() throws Exception {
		
	    errorInfo = new ErrorInfo("100", "Error message", "Detail error message", "400");
		errorInfo.setDetailErrorMessage("Detail Error Message");
		errorInfo.setErrorCode("100");
		errorInfo.setErrorMessage("Error Message");
		psd2SecurityException = new PSD2SecurityException("Error Found",errorInfo);
		errorCodeEnum = SCAConsentErrorCodeEnum.INVALID_REQUEST;
	}
	@Test
	public void test(){
		psd2SecurityException.getErrorInfo();
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void testPopulatePSD2SecurityException(){
		psd2SecurityException.populatePSD2SecurityException(errorCodeEnum);
	}
	@SuppressWarnings("static-access")
	@Test
	public void testpopulatePSD2SecurityExceptionWithDetailMessage(){
		psd2SecurityException.populatePSD2SecurityException(errorInfo.getDetailErrorMessage(), errorCodeEnum);
	}
	@Test
	public void testGetter(){
		assertEquals(errorInfo.getErrorCode(),psd2SecurityException.getErrorInfo().getErrorCode());
	}
	@Test
	public void testConstructor(){
		psd2SecurityException =new PSD2SecurityException(errorInfo.getDetailErrorMessage(), errorInfo);
		assertEquals(errorInfo.getErrorCode(),psd2SecurityException.getErrorInfo().getErrorCode());
	}
	
	
}
