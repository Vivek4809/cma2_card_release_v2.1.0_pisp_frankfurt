/*package com.capgemini.psd2.security.test.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.security.models.JwtAuthenticationToken;
import com.capgemini.psd2.security.models.RawAccessJwtToken;
import com.capgemini.psd2.security.models.UserContext;

public class JwtAuthenticationTokenTest {

	RequestHeaderAttributes requestCorrelation = null;
	RawAccessJwtToken rawAccessJwtToken = null;
	List<GrantedAuthority> authorities = null;

	@Test
	public void twoArgumentConstructorTest() {
		requestCorrelation = new RequestHeaderAttributes();
		requestCorrelation.setCorrelationId("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
	
		rawAccessJwtToken = new RawAccessJwtToken("token", requestCorrelation);
		JwtAuthenticationToken jwtToken = new JwtAuthenticationToken(rawAccessJwtToken);

		assertEquals("token", ((RawAccessJwtToken) jwtToken.getCredentials()).getToken());

		jwtToken.eraseCredentials();
		assertNull(jwtToken.getCredentials());
	}

	@Test
	public void threeArgumentConstructorTest() {
		boolean thrown = false;
		authorities = new ArrayList<>();
		GrantedAuthority grantedAuthority = new GrantedAuthority() {

			@Override
			public String getAuthority() {
				return "authority";
			}
		};
		authorities.add(grantedAuthority);
		JwtAuthenticationToken jwtToken = new JwtAuthenticationToken(UserContext.create("boi123", authorities),
				authorities);
		assertEquals("boi123", (String) jwtToken.getPrincipal());

		try {
			jwtToken.setAuthenticated(true);
		} catch (IllegalArgumentException e) {
			thrown = true;
		}
		assertTrue(thrown);
	}
}
*/