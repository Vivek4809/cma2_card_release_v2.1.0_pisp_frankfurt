/*package com.capgemini.psd2.utilities;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.security.models.UserContext;
import com.capgemini.psd2.security.utilities.JwtTokenUtility;

public class JwtTokenUtilityTest {

	@InjectMocks
	private JwtTokenUtility utility;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	@SuppressWarnings("static-access")
	public void testCreateAccessJwtToken(){
		UserContext userContext = mock(UserContext.class);
		assertNotNull(utility.createAccessJwtToken(userContext, "tokenIssuer", 24, "0123456789abcdef"));
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void testCreateSessionJwtToken(){
		assertNotNull(utility.createSessionJwtToken("tokenIssuer", 24, "0123456789abcdef"));
	}
}
*/