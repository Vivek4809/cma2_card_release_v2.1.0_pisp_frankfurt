package com.capgemini.psd2.pisp.payment.setup.test.transformer.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.payment.setup.transformer.impl.DPaymentConsentsResponseTransformerImpl;
import com.capgemini.psd2.pisp.utilities.PispDateUtility;
import com.capgemini.psd2.pisp.validation.adapter.constants.PaymentSetupConstants;

@RunWith(SpringJUnit4ClassRunner.class)
public class DPaymentConsentsResponseTransformerTest {

	@InjectMocks
	DPaymentConsentsResponseTransformerImpl transformer;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private PispDateUtility pispDateUtility;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testDPaymentConsentsResponseTransformer() {
		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();

		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();
		response.setData(data);
		data.setConsentId("1234");
		response.getData().setCreationDateTime("2017-06-05T15:15:13+00:00");
		resource.setStatus("ACCEPTEDTECHNICALVALIDATION");
		resource.setPaymentConsentId("1234");
		resource.setStatusUpdateDateTime("2017-06-05T15:18:13+00:00");
		resource.setCreatedAt("");

		OBRisk1 risk = new OBRisk1();
		response.setRisk(risk);
		OBDomestic1 initiation = new OBDomestic1();
		data.setInitiation(initiation);

		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setSchemeName("scheme");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		debtorAccount.setSchemeName("schemeName");

		response.setLinks(null);
		PaymentSetupPOSTResponseLinks links = new PaymentSetupPOSTResponseLinks();
		links.getSelf();

		PaymentSetupPOSTResponseMeta meta = new PaymentSetupPOSTResponseMeta();
		meta.setTotalPages(100);
		meta.getTotalPages();
		Mockito.when(reqHeaderAtrributes.getSelfUrl()).thenReturn("/testSelfUrl");
		CustomDPaymentConsentsPOSTResponse x = transformer.paymentConsentsResponseTransformer(response, resource,
				"GET");
		assertEquals(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, x.getData().getStatus());
		assertEquals("2017-06-05T15:18:13+00:00", x.getData().getStatusUpdateDateTime());
	}

	@Test
	public void DPaymentConsentsResponseTransformerTest3() {
		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();

		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();
		response.setData(data);
		data.setConsentId("1234");
		response.getData().setCreationDateTime("2017-06-05T15:15:13+00:00");
		resource.setStatus("ACCEPTEDTECHNICALVALIDATION");
		resource.setPaymentConsentId("1234");
		resource.setStatusUpdateDateTime("2017-06-05T15:18:13+00:00");
		resource.setCreatedAt("");
		resource.setTppDebtorDetails("true");
		resource.setTppDebtorNameDetails("1234");
		OBRisk1 risk = new OBRisk1();
		response.setRisk(risk);
		OBDomestic1 initiation = new OBDomestic1();
		data.setInitiation(initiation);

		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setSchemeName("scheme");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		debtorAccount.setSchemeName("schemeName");

		response.setLinks(null);
		PaymentSetupPOSTResponseLinks links = new PaymentSetupPOSTResponseLinks();
		links.getSelf();

		PaymentSetupPOSTResponseMeta meta = new PaymentSetupPOSTResponseMeta();
		meta.setTotalPages(100);
		meta.getTotalPages();
		Mockito.when(reqHeaderAtrributes.getSelfUrl()).thenReturn("/testSelfUrl");
		CustomDPaymentConsentsPOSTResponse x = transformer.paymentConsentsResponseTransformer(response, resource,
				"GET");
		assertEquals(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, x.getData().getStatus());
		assertEquals("2017-06-05T15:18:13+00:00", x.getData().getStatusUpdateDateTime());
	}

	@Test
	public void DPaymentConsentsResponseTransformerTest2() {
		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();

		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();
		response.setData(data);
		data.setConsentId("1234");
		response.getData().setCreationDateTime("2017-06-05T15:15:13+00:00");
		resource.setStatus("ACCEPTEDTECHNICALVALIDATION");
		resource.setPaymentConsentId("1234");
		resource.setStatusUpdateDateTime("2017-06-05T15:18:13+00:00");
		resource.setCreatedAt("");

		OBRisk1 risk = new OBRisk1();
		response.setRisk(risk);
		OBDomestic1 initiation = new OBDomestic1();
		data.setInitiation(initiation);

		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setSchemeName("scheme");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		debtorAccount.setSchemeName("schemeName");

		response.setLinks(null);
		response.setMeta(null);

		PaymentSetupConstants.schemeNameList.add("schemeName");
		PaymentSetupConstants.schemeNameList.add("scheme");
		Mockito.when(reqHeaderAtrributes.getSelfUrl()).thenReturn("/testSelfUrl");
		CustomDPaymentConsentsPOSTResponse x = transformer.paymentConsentsResponseTransformer(response, resource,
				"GET");
		assertEquals(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, x.getData().getStatus());
		assertEquals("2017-06-05T15:18:13+00:00", x.getData().getStatusUpdateDateTime());
	}

	@Test
	public void testpaymentConsentRequestTransformer() {
		CustomDPaymentConsentsPOSTRequest request = new CustomDPaymentConsentsPOSTRequest();
		request.setData(new OBWriteDataDomesticConsent1());
		request.getData().setAuthorisation(new OBAuthorisation1());
		request.getData().getAuthorisation().setCompletionDateTime("2019-12-31T00:00:00+00:00");

		when(pispDateUtility.transformDateTimeInRequest(anyString())).thenReturn("2019-12-31T00:00:00+00:00");
		transformer.paymentConsentRequestTransformer(request);
	}

	@After
	public void tearDown() throws Exception {
		transformer = null;
		reqHeaderAtrributes = null;
	}

}