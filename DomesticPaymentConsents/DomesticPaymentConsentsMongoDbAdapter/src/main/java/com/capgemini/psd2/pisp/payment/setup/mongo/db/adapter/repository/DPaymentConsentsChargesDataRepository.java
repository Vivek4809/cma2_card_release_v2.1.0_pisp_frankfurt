package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.capgemini.psd2.pisp.domain.OBCharge1;

public interface DPaymentConsentsChargesDataRepository extends MongoRepository<OBCharge1,String>{
	
}
