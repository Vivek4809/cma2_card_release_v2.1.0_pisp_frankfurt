package com.capgemini.psd2.account.statements.routing.adapter.routing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.AccountStatementsAdapter;

	@Component
	public class AccountStatementsCoreSystemAdapterFactory implements ApplicationContextAware, AccountStatementsAdapterFactory{

		
		/** The application context. */
		private ApplicationContext applicationContext;

		@Override
		public AccountStatementsAdapter getAdapterInstance(String adapterName) {
			return (AccountStatementsAdapter) applicationContext.getBean(adapterName);
			
		}

		@Override
		public void setApplicationContext(ApplicationContext context) {
			this.applicationContext = context;
			
		}
		
		/* (non-Javadoc)
		 * @see com.capgemini.psd2.account.information.routing.adapter.routing.AccountInformationAdapterFactory#getAdapterInstance(java.lang.String)
		 */
}

