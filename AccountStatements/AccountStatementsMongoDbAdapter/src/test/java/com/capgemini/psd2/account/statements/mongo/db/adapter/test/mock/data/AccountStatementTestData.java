package com.capgemini.psd2.account.statements.mongo.db.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount;
import com.capgemini.psd2.aisp.domain.OBExternalStatementType1Code;
/*import com.capgemini.psd2.aisp.domain.OBStatement1Amount;
import com.capgemini.psd2.aisp.domain.OBStatement1Amount1;
import com.capgemini.psd2.aisp.domain.OBStatement1Amount2;
import com.capgemini.psd2.aisp.domain.OBStatement1Amount3;*/
import com.capgemini.psd2.aisp.domain.OBStatementAmount1;
import com.capgemini.psd2.aisp.domain.OBStatementBenefit1;
import com.capgemini.psd2.aisp.domain.OBStatementDateTime1;
import com.capgemini.psd2.aisp.domain.OBStatementFee1;
import com.capgemini.psd2.aisp.domain.OBStatementFee1.CreditDebitIndicatorEnum;
import com.capgemini.psd2.aisp.domain.OBStatementInterest1;
import com.capgemini.psd2.aisp.domain.OBStatementRate1;
import com.capgemini.psd2.aisp.domain.OBStatementValue1;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountStatementsCMA2;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;


public class AccountStatementTestData {

	static AccountStatementsCMA2 data = new AccountStatementsCMA2();
	static OBStatementAmount1 statementAmount = new OBStatementAmount1();
	static OBActiveOrHistoricCurrencyAndAmount benefitAmount = new OBActiveOrHistoricCurrencyAndAmount();
	static OBStatementBenefit1 benefit = new OBStatementBenefit1();
	static OBStatementFee1 statementFee = new OBStatementFee1();
	static OBActiveOrHistoricCurrencyAndAmount statementAmount1 = new OBActiveOrHistoricCurrencyAndAmount();
	static OBStatementInterest1 statementInterest = new OBStatementInterest1();
	static OBActiveOrHistoricCurrencyAndAmount interestAmount = new OBActiveOrHistoricCurrencyAndAmount();
	static OBStatementDateTime1 statementDateTime = new OBStatementDateTime1();
	static OBStatementRate1 statementRate = new OBStatementRate1();
	static OBStatementValue1 statementValue = new OBStatementValue1();
	static OBActiveOrHistoricCurrencyAndAmount statementAmount3 = new OBActiveOrHistoricCurrencyAndAmount();
	
	public static String getTestAccountId() {
		return "269c3ff5-d7f8-419b-a3b9-7136c5b4611a";
	}

	public static String getTestBankID() {
		return "TestBankID";
	}
	
	public static AccountMapping getMockAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		mapping.setTppCID("tpp123");
		mapping.setPsuId("user123");
		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();
		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("SC802001");
		accountRequest.setAccountNumber("10203345");
		selectedAccounts.add(accountRequest);
		mapping.setAccountDetails(selectedAccounts);
		return mapping;
	}
	
	public static Page<AccountStatementsCMA2> getAccountStatementsDataPage(){
		List<AccountStatementsCMA2> dataList = new ArrayList<AccountStatementsCMA2>();
		
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setPsuId("88888888");
		data.setAccountNumber("10203345");
		data.setAccountNSC("SC802001");
		data.setStatementId("8sfhke-sifhkeuf-97819");
		data.setStatementReference("002");
		data.setType(OBExternalStatementType1Code.REGULARPERIODIC);
		data.setStartDateTime("2020-07-31T00:00:00+00:00");
		data.setEndDateTime("2021-12-31T23:59:59+00:00");
		data.setCreationDateTime("2017-09-01T00:00:00+00:00");
		List<String> statementDescription = new ArrayList<String>();
		statementDescription.add("August 2017 Statement");
		data.setStatementDescription(statementDescription);
		List<OBStatementBenefit1> benefitList = new ArrayList<OBStatementBenefit1>();
		benefitAmount.setAmount("10.00");
		benefitAmount.setCurrency("GBP");
		benefit.setAmount(benefitAmount);
		benefit.setType("CASHBACK");
		benefitList.add(benefit);
		data.setStatementBenefit(benefitList);
		List<OBStatementFee1> statementFeeList = new ArrayList<OBStatementFee1>();
		statementAmount1.setAmount("65.98");
		statementAmount1.setCurrency("GBP");
		statementFee.setAmount(statementAmount1);
		statementFee.setCreditDebitIndicator(CreditDebitIndicatorEnum.CREDIT);
		statementFee.setType("ANNUAL");
		statementFeeList.add(statementFee);
		data.setStatementFee(statementFeeList);
		List<OBStatementInterest1> statementInterestList = new ArrayList<OBStatementInterest1>();
		interestAmount.setAmount("45.09");
		interestAmount.setCurrency("GBP");
		statementInterest.setAmount(interestAmount);
		statementInterest.setCreditDebitIndicator(OBStatementInterest1.CreditDebitIndicatorEnum.CREDIT);
		statementInterest.setType("BALANCETRANSFER");
		statementInterestList.add(statementInterest);
		data.setStatementInterest(statementInterestList);
		List<OBStatementDateTime1> statementdateTimeList = new ArrayList<OBStatementDateTime1>();
		statementDateTime.setDateTime("2018-07-23T10:29:22.765Z");
		statementDateTime.setType("BALANCETRANSFERPROMOEND");
		statementdateTimeList.add(statementDateTime);
		data.setStatementDateTime(statementdateTimeList);
		List<OBStatementRate1> statementRateList = new ArrayList<OBStatementRate1>();
		statementRate.setRate("1.5");
		statementRate.setType("ANNUALBALANCETRANSFER");
		statementRateList.add(statementRate);
		data.setStatementRate(statementRateList);
		List<OBStatementValue1> statementValueList = new ArrayList<OBStatementValue1>();
		statementValue.setValue(0);
		statementValue.setType("AIRMILESPOINTS");
		statementValueList.add(statementValue);
		data.setStatementValue(statementValueList);
		List<OBStatementAmount1> statementAmountList = new ArrayList<OBStatementAmount1>();
		statementAmount3.setAmount("400.00");
		statementAmount3.setCurrency("GBP");
		statementAmount.setAmount(statementAmount3);
		statementAmount.setCreditDebitIndicator(OBStatementAmount1.CreditDebitIndicatorEnum.CREDIT);
		statementAmount.setType("CLOSINGBALANCE");
		statementAmountList.add(statementAmount);
		data.setStatementAmount(statementAmountList);
		dataList.add(data);
		Page<AccountStatementsCMA2> page = new PageImpl<>(dataList);
		return page;
	}
}
