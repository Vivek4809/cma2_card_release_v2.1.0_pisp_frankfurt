package com.capgemini.psd2.pisp.international.standing.order.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.amazonaws.HttpMethod;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.adapter.InternationalStandingOrderAdapter;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.international.standing.order.comparator.InternationalStandingOrdersPayloadComparator;
import com.capgemini.psd2.pisp.international.standing.order.service.InternationalStandingOrdersService;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentSubmissionProcessingAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalStandingOrdersPaymentStagingAdapter;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Lazy
@Service
public class InternationalStandingOrdersServiceImpl implements InternationalStandingOrdersService{
	@Autowired
	private PaymentSubmissionProcessingAdapter<CustomIStandingOrderPOSTRequest, CustomIStandingOrderPOSTResponse> paymentsProcessingAdapter;
	
	@Autowired
	private InternationalStandingOrdersPayloadComparator comparator;
	
	@Autowired
	@Qualifier("iStandingOrderConsentsStagingRoutingAdapter")
	private InternationalStandingOrdersPaymentStagingAdapter iPaymentStagingAdapter;
	
	@Autowired
	@Qualifier("iStandingOrderRoutingAdapter")
	private InternationalStandingOrderAdapter iStandingOrderAdapter;
	
	@Value("${cmaVersion}")
	private String cmaVersion;
	
	@Override
	public CustomIStandingOrderPOSTResponse createInternationalStandingOrderResource(
			CustomIStandingOrderPOSTRequest request) {
		CustomIStandingOrderPOSTResponse customIStandingOrderPOSTResponse = null;
		CustomIStandingOrderConsentsPOSTResponse consentsFoundationResource = null;
		PaymentConsentsPlatformResource paymentConsentPlatformResource = null;
		PaymentsPlatformResource paymentsPlatformResource = null;	
		
		Map<String, Object> paymentsPlatformResourceMap = paymentsProcessingAdapter.preSubmissionProcessing(request,
				populatePlatformDetails(), request.getData().getConsentId().trim());
		
		paymentConsentPlatformResource = (PaymentConsentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.CONSENT);
		paymentsPlatformResource = (PaymentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.SUBMISSION);
		consentsFoundationResource = internationalPayLoadCompare(request, paymentConsentPlatformResource,
				request.getData().getConsentId().trim());
		
		if (paymentsPlatformResource == null) {
			paymentsPlatformResource = paymentsProcessingAdapter.createInitialPaymentsPlatformResource(
					request.getData().getConsentId(), cmaVersion,
					PaymentTypeEnum.INTERNATIONAL_ST_ORD);
			paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);
		}
		
		if (paymentsPlatformResource.getProccessState().equals(PaymentConstants.INCOMPLETE)) {
			populateInternationalStagedProcessingFields(request, consentsFoundationResource);
			Map<String, OBExternalStatus1Code> paymentStatusMap = new HashMap<>();
			paymentStatusMap.put(PaymentConstants.INITIAL, OBExternalStatus1Code.INITIATIONPENDING);
			paymentStatusMap.put(PaymentConstants.FAIL, OBExternalStatus1Code.INITIATIONFAILED);
			paymentStatusMap.put(PaymentConstants.PASS_M_AUTH, OBExternalStatus1Code.INITIATIONCOMPLETED);
			paymentStatusMap.put(PaymentConstants.PASS_M_AWAIT, OBExternalStatus1Code.INITIATIONPENDING);
			paymentStatusMap.put(PaymentConstants.PASS_M_REJECT, OBExternalStatus1Code.INITIATIONFAILED);
			request.setCreatedOn(paymentsPlatformResource.getCreatedAt());
			customIStandingOrderPOSTResponse = iStandingOrderAdapter.processInternationalStandingOrder(request, paymentStatusMap,
					null);
		}
		
		if (paymentsPlatformResource.getProccessState().equals(PaymentConstants.COMPLETED)) {
			CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
			identifiers.setPaymentSetupVersion(cmaVersion);
			identifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);
			identifiers.setPaymentConsentId(paymentsPlatformResource.getPaymentConsentId());
			identifiers.setPaymentSubmissionId(paymentsPlatformResource.getSubmissionId());
			customIStandingOrderPOSTResponse = iStandingOrderAdapter.retrieveStagedInternationalStandingOrder(identifiers, null);
		}
		if (customIStandingOrderPOSTResponse == null) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_NO_PAYMENT_SETUP_RESOURCES_FOUND));
		}

		OBExternalStatus2Code multiAuthStatus = OBExternalStatus2Code.AUTHORISED;
		if (customIStandingOrderPOSTResponse.getData()!= null)
			multiAuthStatus = customIStandingOrderPOSTResponse.getData().getMultiAuthorisation().getStatus();

		return (CustomIStandingOrderPOSTResponse) paymentsProcessingAdapter.postPaymentProcessFlows(
				paymentsPlatformResourceMap, customIStandingOrderPOSTResponse,
				customIStandingOrderPOSTResponse.getProcessExecutionStatus(),
				customIStandingOrderPOSTResponse.getData().getInternationalStandingOrderId(), multiAuthStatus,
				HttpMethod.POST.toString());
	}
	
	private CustomPaymentSetupPlatformDetails populatePlatformDetails() {
		CustomPaymentSetupPlatformDetails customPaymentSetupPlatformDetails = new CustomPaymentSetupPlatformDetails();
		customPaymentSetupPlatformDetails.setSetupCmaVersion(cmaVersion);
		customPaymentSetupPlatformDetails.setPaymentType(PaymentTypeEnum.DOMESTIC_ST_ORD);
		return customPaymentSetupPlatformDetails;
	}
	private CustomIStandingOrderConsentsPOSTResponse internationalPayLoadCompare(
			CustomIStandingOrderPOSTRequest submissionRequest,
			PaymentConsentsPlatformResource paymentConsentPlatformResource, String consentId) {

		CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
		identifiers.setPaymentConsentId(consentId);
		identifiers.setPaymentSetupVersion(cmaVersion);
		identifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_ST_ORD);

		CustomIStandingOrderConsentsPOSTResponse paymentSetupFoundationResponse = iPaymentStagingAdapter
				.retrieveStagedInternationalStandingOrdersConsents(identifiers, null);

		if (comparator.comparePaymentDetails(paymentSetupFoundationResponse, submissionRequest,
				paymentConsentPlatformResource) > 0)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_CONSENTMISMATCH,
					ErrorMapKeys.RESOURCE_CONSENT_MISMATCH));
		return paymentSetupFoundationResponse;
	}
	
	private CustomIStandingOrderPOSTRequest populateInternationalStagedProcessingFields(
			CustomIStandingOrderPOSTRequest submissionRequest,
			CustomIStandingOrderConsentsPOSTResponse paymentSetupFoundationResponse) {
		/*
		 * If TPP has not sent DebtorAccount.Name then product will set the name
		 * details from staging record which is sent by FS
		 */
		if (submissionRequest.getData().getInitiation().getDebtorAccount() != null && NullCheckUtils
				.isNullOrEmpty(submissionRequest.getData().getInitiation().getDebtorAccount().getName())) {

			submissionRequest.getData().getInitiation().getDebtorAccount()
					.setName(paymentSetupFoundationResponse.getData().getInitiation().getDebtorAccount().getName());
		}
		/*
		 * If debtor details are not provided by TPP then Platform will set
		 * debtor details received from FS. Same will be sent to FS for
		 * Pre-SubmissionValidation and Account Permission
		 */

		if (submissionRequest.getData().getInitiation().getDebtorAccount() == null) {
			submissionRequest.getData().getInitiation()
					.setDebtorAccount(paymentSetupFoundationResponse.getData().getInitiation().getDebtorAccount());
		}
		submissionRequest.setFraudSystemResponse(paymentSetupFoundationResponse.getFraudScore());
		return submissionRequest;
	}

	@Override
	public CustomIStandingOrderPOSTResponse retrieveInternationalStandingOrderResource(String submissionId) {
		Map<String, Object> paymentsPlatformResourceMap = paymentsProcessingAdapter
				.prePaymentProcessGETFlows(submissionId, PaymentTypeEnum.DOMESTIC_ST_ORD);
		PaymentsPlatformResource paymentsPlatformResource = (PaymentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.SUBMISSION);
		String paymentConsentId = paymentsPlatformResource.getPaymentConsentId();
		String paymentSetUpVersion = cmaVersion;
		

		CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
		identifiers.setPaymentSetupVersion(paymentSetUpVersion);
		identifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);
		identifiers.setPaymentConsentId(paymentConsentId);
		identifiers.setPaymentSubmissionId(submissionId);
		CustomIStandingOrderPOSTResponse paymentsFoundationResponse = iStandingOrderAdapter
				.retrieveStagedInternationalStandingOrder(identifiers, null);

		if (paymentsFoundationResponse == null) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
					ErrorMapKeys.NO_PAYMENT_SETUP_RESOURCES_FOUND));
		}
		OBExternalStatus2Code multiAuthStatus = null;

		if (paymentsFoundationResponse.getData().getMultiAuthorisation() != null)
			multiAuthStatus = paymentsFoundationResponse.getData().getMultiAuthorisation().getStatus();

		return (CustomIStandingOrderPOSTResponse) paymentsProcessingAdapter.postPaymentProcessFlows(
				paymentsPlatformResourceMap, paymentsFoundationResponse,
				paymentsFoundationResponse.getProcessExecutionStatus(),
				paymentsFoundationResponse.getData().getInternationalStandingOrderId(), multiAuthStatus,
				HttpMethod.GET.toString());
	}
	
}
