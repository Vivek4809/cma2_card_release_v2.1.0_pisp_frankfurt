
package com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.client;

import org.springframework.http.HttpHeaders;

import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.rest.client.model.RequestInfo;

public interface ValidatePaymentFoundationServiceClient {

	public ValidationPassed validatePayment(RequestInfo requestInfo, PaymentInstruction paymentInstruction, Class<ValidationPassed> response, HttpHeaders httpHeaders);

}
