package com.capgemini.psd2.pisp.payment.setup.mongo.db.test.adapter.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExchangeRate1;
import com.capgemini.psd2.pisp.domain.OBExchangeRate2;
import com.capgemini.psd2.pisp.domain.OBExchangeRateType2Code;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBInternationalScheduled1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternational1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduledConsent1;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl.ISPConsentsStagingMongoDbAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.ISPConsentsFoundationRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class ISPConsentsStagingMongoDbAdapterImplTest {

	@Mock
	private ISPConsentsFoundationRepository repository;

	@Mock
	private SandboxValidationUtility utility;

	@Mock
	private RequestHeaderAttributes reqAttributes;

	@InjectMocks
	private ISPConsentsStagingMongoDbAdapterImpl adapter;

	@InjectMocks
	private List<OBCharge1> chargesList = new ArrayList<OBCharge1>();

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testProcessInternationalScheduledPaymentConsents() {
		CustomISPConsentsPOSTRequest request = ISPConsentsStagingMongoDbAdapterImplTestMockData.getRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		OBExchangeRate2 information = new OBExchangeRate2();

		when(utility.getMockedConsentProcessExecStatus(anyString(), anyString()))
				.thenReturn(ProcessConsentStatusEnum.PASS);
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);
		when(utility.isValidSecIdentification(anyString())).thenReturn(false);
		when(utility.getMockedExchangeRateInformation(any(), anyString())).thenReturn(information);

		when(repository.save(any(CustomISPConsentsPOSTResponse.class))).thenReturn(response);
		adapter.processInternationalScheduledPaymentConsents(request, stageIdentifiers, new HashMap<>(),
				OBExternalConsentStatus1Code.AUTHORISED, OBExternalConsentStatus1Code.REJECTED);
	}

	@Test
	public void testProcessInternationalScheduledPaymentConsents_NullConsentId() {
		CustomISPConsentsPOSTRequest request = ISPConsentsStagingMongoDbAdapterImplTestMockData.getRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		OBExchangeRate2 information = new OBExchangeRate2();

		when(utility.isValidSecIdentification(anyString())).thenReturn(false);
		when(utility.getMockedExchangeRateInformation(any(), anyString())).thenReturn(information);

		when(repository.save(any(CustomISPConsentsPOSTResponse.class))).thenReturn(response);
		adapter.processInternationalScheduledPaymentConsents(request, stageIdentifiers, new HashMap<>(),
				OBExternalConsentStatus1Code.AUTHORISED, OBExternalConsentStatus1Code.REJECTED);
	}

	@Test(expected = PSD2Exception.class)
	public void testProcessInternationalScheduledPaymentConsents_ValidAmount() {
		CustomISPConsentsPOSTRequest request = ISPConsentsStagingMongoDbAdapterImplTestMockData.getRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		OBExchangeRate2 information = new OBExchangeRate2();

		when(utility.getMockedConsentProcessExecStatus(anyString(), anyString()))
				.thenReturn(ProcessConsentStatusEnum.PASS);
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);
		when(utility.isValidSecIdentification(anyString())).thenReturn(false);
		when(utility.getMockedExchangeRateInformation(any(), anyString())).thenReturn(information);

		when(repository.save(any(CustomISPConsentsPOSTResponse.class))).thenReturn(response);
		adapter.processInternationalScheduledPaymentConsents(request, stageIdentifiers, new HashMap<>(),
				OBExternalConsentStatus1Code.AUTHORISED, OBExternalConsentStatus1Code.REJECTED);
	}

	@Test(expected = PSD2Exception.class)
	public void testProcessInternationalScheduledPaymentConsents_ValidSecondaryIdentification() {
		CustomISPConsentsPOSTRequest request = ISPConsentsStagingMongoDbAdapterImplTestMockData.getRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		OBExchangeRate2 information = new OBExchangeRate2();

		when(utility.getMockedConsentProcessExecStatus(anyString(), anyString()))
				.thenReturn(ProcessConsentStatusEnum.PASS);
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);
		when(utility.isValidSecIdentification(anyString())).thenReturn(true);
		when(utility.getMockedExchangeRateInformation(any(), anyString())).thenReturn(information);

		when(repository.save(any(CustomISPConsentsPOSTResponse.class))).thenReturn(response);
		adapter.processInternationalScheduledPaymentConsents(request, stageIdentifiers, new HashMap<>(),
				OBExternalConsentStatus1Code.AUTHORISED, OBExternalConsentStatus1Code.REJECTED);
	}

	@Test(expected = PSD2Exception.class)
	public void testProcessInternationalScheduledPaymentConsents_DataAccessFailureException() {
		CustomISPConsentsPOSTRequest request = ISPConsentsStagingMongoDbAdapterImplTestMockData.getRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		OBExchangeRate2 information = new OBExchangeRate2();

		when(utility.getMockedConsentProcessExecStatus(anyString(), anyString()))
				.thenReturn(ProcessConsentStatusEnum.FAIL);
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);
		when(utility.isValidSecIdentification(anyString())).thenReturn(false);
		when(utility.getMockedExchangeRateInformation(any(), anyString())).thenReturn(information);

		when(repository.save(any(CustomISPConsentsPOSTResponse.class)))
				.thenThrow(new DataAccessResourceFailureException("Test"));
		adapter.processInternationalScheduledPaymentConsents(request, stageIdentifiers, new HashMap<>(),
				OBExternalConsentStatus1Code.AUTHORISED, OBExternalConsentStatus1Code.REJECTED);
	}

	@Test
	public void testRetrieveStagedInternationalScheduledPaymentConsents() {
		CustomISPConsentsPOSTResponse response = ISPConsentsStagingMongoDbAdapterImplTestMockData.getResponse();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		when(repository.findOneByDataConsentId(anyString())).thenReturn(response);
		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);
		
		adapter.retrieveStagedInternationalScheduledPaymentConsents(stageIdentifiers, new HashMap<>());
	}
	
	@Test
	public void testRetrieveStagedInternationalScheduledPaymentConsents_Post() {
		CustomISPConsentsPOSTResponse response = ISPConsentsStagingMongoDbAdapterImplTestMockData.getResponse();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		when(repository.findOneByDataConsentId(anyString())).thenReturn(response);
		when(reqAttributes.getMethodType()).thenReturn("POST");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);
		
		adapter.retrieveStagedInternationalScheduledPaymentConsents(stageIdentifiers, new HashMap<>());
	}
	
	@Test(expected=PSD2Exception.class)
	public void retrieveStagedInternationalScheduledPaymentConsents_Exception() {
		CustomISPConsentsPOSTResponse response = null;
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		when(repository.findOneByDataConsentId(anyString())).thenReturn(response);
		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);
		
		adapter.retrieveStagedInternationalScheduledPaymentConsents(stageIdentifiers, new HashMap<>());
	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveStagedInternationalScheduledPaymentConsents_Exception() {
		CustomISPConsentsPOSTResponse response = ISPConsentsStagingMongoDbAdapterImplTestMockData.getResponse();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		when(repository.findOneByDataConsentId(anyString())).thenReturn(response);
		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);
		
		adapter.retrieveStagedInternationalScheduledPaymentConsents(stageIdentifiers, new HashMap<>());
	}
	
	@Test
	public void createstage_Test_CIExRate() {

		CustomISPConsentsPOSTRequest request = new CustomISPConsentsPOSTRequest();
		OBWriteDataInternationalScheduledConsent1 data = new OBWriteDataInternationalScheduledConsent1();
		request.setData(data);
		OBInternationalScheduled1 initiation = new OBInternationalScheduled1();
		data.setInitiation(initiation);
		OBChargeBearerType1Code chargeBearer = null;
		initiation.setChargeBearer(chargeBearer.SHARED);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSecondaryIdentification("Test");
		initiation.setCreditorAccount(creditorAccount);
		
		ReflectionTestUtils.setField(adapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		initiation.setExchangeRateInformation(exchangeRateInformation);
		initiation.setCurrencyOfTransfer("USD");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("USD");
		initiation.setInstructedAmount(instructedAmount);

		OBCharge1 charge = new OBCharge1();
		OBCharge1Amount x = new OBCharge1Amount();
		x.setAmount("11.11");
		x.setCurrency("USD");
		charge.setAmount(x);
		charge.setChargeBearer(OBChargeBearerType1Code.SHARED);
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		adapter.processInternationalScheduledPaymentConsents(request, stageIdentifiers, new HashMap<>(),
				OBExternalConsentStatus1Code.AUTHORISED, OBExternalConsentStatus1Code.REJECTED);

	}
	
	@Test
	public void createstage_Test_CIExRate_NonMatching() {

		CustomISPConsentsPOSTRequest request = new CustomISPConsentsPOSTRequest();
		OBWriteDataInternationalScheduledConsent1 data = new OBWriteDataInternationalScheduledConsent1();
		request.setData(data);
		OBInternationalScheduled1 initiation = new OBInternationalScheduled1();
		data.setInitiation(initiation);
		OBChargeBearerType1Code chargeBearer = null;
		initiation.setChargeBearer(chargeBearer.SHARED);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSecondaryIdentification("Test");
		initiation.setCreditorAccount(creditorAccount);
		ReflectionTestUtils.setField(adapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		initiation.setExchangeRateInformation(exchangeRateInformation);
		initiation.setCurrencyOfTransfer("USD");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("USD");
		initiation.setInstructedAmount(instructedAmount);
		OBCharge1 charge = new OBCharge1();
		OBCharge1Amount x = new OBCharge1Amount();
		x.setAmount("11.11");
		x.setCurrency("USD");
		charge.setAmount(x);
		charge.setChargeBearer(OBChargeBearerType1Code.SHARED);
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		adapter.processInternationalScheduledPaymentConsents(request, stageIdentifiers, new HashMap<>(),
				OBExternalConsentStatus1Code.AUTHORISED, OBExternalConsentStatus1Code.REJECTED);
	}


	@After
	public void tearDown() {
		repository = null;
		utility = null;
		reqAttributes = null;
		adapter = null;
	}

}
