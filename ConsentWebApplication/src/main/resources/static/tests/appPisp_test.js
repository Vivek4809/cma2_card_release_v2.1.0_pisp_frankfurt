"use strict";
describe("ConsentApp.appPisp_test", function() {
	 var $rootScope, e, config, $location;
	    beforeEach(module("consentApp"));
	    beforeEach(function() {
	        appendSetFixtures('<input type="hidden" id="consentType" name="consentType" value="PISP"/>');
	    });
	    beforeEach(inject(function(_$rootScope_, _config_, _$location_) {
	        $rootScope = _$rootScope_;
	        config = _config_;
	        $location = _$location_;
	        spyOn($location, "path").and.returnValue("/");
	    }));
	    describe("test all", function() {
	        it("should set PISP as consent type  in config", function() {
	            expect(config.applicationType).toEqual("/pisp-account");
	        });

	    });
	});