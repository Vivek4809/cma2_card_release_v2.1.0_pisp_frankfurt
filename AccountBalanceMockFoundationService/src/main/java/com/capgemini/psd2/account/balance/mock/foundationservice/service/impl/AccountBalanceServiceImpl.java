/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.mock.foundationservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.balance.mock.foundationservice.domain.Accnt;
import com.capgemini.psd2.account.balance.mock.foundationservice.domain.Accounts;
import com.capgemini.psd2.account.balance.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.account.balance.mock.foundationservice.repository.AccountBalanceRepository;
import com.capgemini.psd2.account.balance.mock.foundationservice.service.AccountBalanceService;

/**
 * The Class AccountBalanceServiceImpl.
 */
@Service
public class AccountBalanceServiceImpl implements AccountBalanceService {

	/** The repository. */
	@Autowired
	private AccountBalanceRepository repository;

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.account.balance.mock.foundationservice.service.AccountBalanceService#retrieveAccountBalance(java.lang.String, java.lang.String)
	 */
	@Override
	public Accounts retrieveAccountBalance(String nsc, String accountNumber)throws Exception {

		Accnt accnt = repository.findByNscAndAccountNumber(nsc, accountNumber);

		if (accnt == null) {
			throw new RecordNotFoundException("Not Found");
		}
		Accounts account = new Accounts();
		account.getAccount().add(accnt);

		return account;

	}

}
