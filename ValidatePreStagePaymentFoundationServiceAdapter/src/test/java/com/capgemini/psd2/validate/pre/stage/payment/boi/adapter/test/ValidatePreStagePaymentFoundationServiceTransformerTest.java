package com.capgemini.psd2.validate.pre.stage.payment.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.constants.ValidatePreStagePaymentFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.transformer.ValidatePreStagePaymentFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CreditorAccount;
import com.capgemini.psd2.pisp.domain.CreditorAgent;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAgent;
import com.capgemini.psd2.pisp.domain.PaymentSetup;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;
import com.capgemini.psd2.pisp.domain.Risk;
import com.capgemini.psd2.pisp.domain.Risk.PaymentContextCodeEnum;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;

@RunWith(SpringJUnit4ClassRunner.class)
public class ValidatePreStagePaymentFoundationServiceTransformerTest {

	@InjectMocks
	ValidatePreStagePaymentFoundationServiceTransformer transformer;
	
	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void transformPaymentSetupPOSTRequestTest1(){
		
		CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest = new CustomPaymentSetupPOSTRequest();
		
	    PaymentSetup data = new PaymentSetup();
	    
	    PaymentSetupInitiation initiation = new PaymentSetupInitiation();
	    initiation.setInstructionIdentification("ANSM023");
	    initiation.setEndToEndIdentification("FRESCO.21302.GFX.37");
	    
	    DebtorAccount debtorAccount = new DebtorAccount();
	    debtorAccount.setIdentification("01234567123654");
	    debtorAccount.setName("Andrea Smith");
	    debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
	    
	    DebtorAgent debtorAgent = new DebtorAgent();
	    debtorAgent.setIdentification("SC112800");
	    debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);        
	    
	    CreditorAccount creditorAccount = new CreditorAccount();
	    creditorAccount.setIdentification("21325698123654");
	    creditorAccount.setName("Bob Clements");
	    creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
	    
	    CreditorAgent creditorAgent = new CreditorAgent();
	    creditorAgent.setIdentification("080800");// In FS side it is expected to be Integer 
	    creditorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum.BICFI);
	    
	    PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
	    instructedAmount.setAmount("20.00");
	    instructedAmount.setCurrency("GBP");
	    
	    RemittanceInformation remittanceInformation = new RemittanceInformation();
	    remittanceInformation.setReference("FRESCO-037");
	    remittanceInformation.setUnstructured("Internal ops code 5120103");
	    
	    RiskDeliveryAddress riskDeliveryAddress = new RiskDeliveryAddress();
	    List<String> addressLine = new ArrayList<>();
	    addressLine.add("test1");
	    addressLine.add("test2");
	    List<String> countrySubDivision = new ArrayList<>();
	    countrySubDivision.add("test1");
	    countrySubDivision.add("test2");
	    
	    riskDeliveryAddress.setAddressLine(addressLine);
	    riskDeliveryAddress.setCountrySubDivision(countrySubDivision);
	    
	    
	    initiation.setCreditorAccount(creditorAccount);
	    initiation.setCreditorAgent(creditorAgent);
	    initiation.setDebtorAccount(debtorAccount);
	    initiation.setDebtorAgent(debtorAgent);
	    initiation.setInstructedAmount(instructedAmount);
	    initiation.setRemittanceInformation(remittanceInformation);
	    

	    Risk risk = new Risk();
	    risk.setPaymentContextCode(PaymentContextCodeEnum.PERSONTOPERSON);
	    risk.setDeliveryAddress(riskDeliveryAddress);
	    
	    data.setInitiation(initiation);
	    paymentSetupPOSTRequest.setData(data);
	    paymentSetupPOSTRequest.setRisk(risk);

	    Map<String, String> params = new HashMap<String, String>();

	    params.put(ValidatePreStagePaymentFoundationServiceConstants.USER_ID, "testUser");
	    params.put(ValidatePreStagePaymentFoundationServiceConstants.CHANNEL_ID, "BOL");
	    params.put(ValidatePreStagePaymentFoundationServiceConstants.CORRELATION_ID, "testCorrelation");
	    params.put(ValidatePreStagePaymentFoundationServiceConstants.PLATFORM_ID, "testPlatform");
	    
	    PaymentInstruction instruction = new PaymentInstruction();
	    
	    
	    instruction = transformer.transformPaymentSetupPOSTRequest(paymentSetupPOSTRequest);
	    assertNotNull(instruction);
	}
	
	@Test
	public void transformPaymentSetupPOSTRequestTest2(){
		
		CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest = new CustomPaymentSetupPOSTRequest();
		
	    PaymentSetup data = new PaymentSetup();
	    
	    PaymentSetupInitiation initiation = new PaymentSetupInitiation();
	    initiation.setInstructionIdentification("ANSM023");
	    initiation.setEndToEndIdentification("FRESCO.21302.GFX.37");
	    
	    DebtorAccount debtorAccount = new DebtorAccount();
	    debtorAccount.setIdentification("01234567");
	    debtorAccount.setName("Andrea Smith");
	    debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);        
	    
	    CreditorAccount creditorAccount = new CreditorAccount();
	    creditorAccount.setIdentification("21325698");
	    creditorAccount.setName("Bob Clements");
	    creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
	    
	    PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
	    instructedAmount.setAmount("20.00");
	    instructedAmount.setCurrency("GBP");
	    
	    RemittanceInformation remittanceInformation = new RemittanceInformation();
	    remittanceInformation.setReference("FRESCO-037");
	    remittanceInformation.setUnstructured("Internal ops code 5120103");
	    
	    RiskDeliveryAddress riskDeliveryAddress = new RiskDeliveryAddress();
	    List<String> addressLine = new ArrayList<>();
	    addressLine.add("test1");
	    addressLine.add("test2");
	    
	    
	    initiation.setCreditorAccount(creditorAccount);
	    initiation.setDebtorAccount(debtorAccount);
	    initiation.setInstructedAmount(instructedAmount);
	    initiation.setRemittanceInformation(remittanceInformation);
	    

	    Risk risk = new Risk();
	    risk.setPaymentContextCode(PaymentContextCodeEnum.PERSONTOPERSON);
	    risk.setDeliveryAddress(riskDeliveryAddress);
	    
	    data.setInitiation(initiation);
	    paymentSetupPOSTRequest.setData(data);
	    paymentSetupPOSTRequest.setRisk(risk);

	    Map<String, String> params = new HashMap<String, String>();

	    params.put(ValidatePreStagePaymentFoundationServiceConstants.USER_ID, "testUser");
	    params.put(ValidatePreStagePaymentFoundationServiceConstants.CHANNEL_ID, "BOL");
	    params.put(ValidatePreStagePaymentFoundationServiceConstants.CORRELATION_ID, "testCorrelation");
	    params.put(ValidatePreStagePaymentFoundationServiceConstants.PLATFORM_ID, "testPlatform");
	    
	    PaymentInstruction instruction = new PaymentInstruction();
	    
	    
	    instruction = transformer.transformPaymentSetupPOSTRequest(paymentSetupPOSTRequest);
	    assertNotNull(instruction);
	}
	
	@Test
	public void transformValidatePreStagePaymentResponseTest1(){
		
		ValidationPassed validationPassed = new ValidationPassed();
		validationPassed.setSuccessCode("testCode");
		validationPassed.setSuccessMessage("testMessage");
		PaymentSetupValidationResponse paymentSetupPreValidationResponse = new PaymentSetupValidationResponse();
		
		
		paymentSetupPreValidationResponse = transformer.transformValidatePreStagePaymentResponse(validationPassed);
		assertNotNull(paymentSetupPreValidationResponse);
	}
	
	@Test
	public void transformValidatePreStagePaymentResponseTest2(){
		
		ValidationPassed validationPassed = new ValidationPassed();
		PaymentSetupValidationResponse paymentSetupPreValidationResponse = transformer.transformValidatePreStagePaymentResponse(validationPassed);
		assertNotNull(paymentSetupPreValidationResponse);
	}
	
	@Test
	public void transformValidatePreStagePaymentResponseValidationPassedNullCheck(){
		PaymentSetupValidationResponse paymentSetupPreValidationResponse = transformer.transformValidatePreStagePaymentResponse(null);
		assertNotNull(paymentSetupPreValidationResponse);
	}
	
	
}
