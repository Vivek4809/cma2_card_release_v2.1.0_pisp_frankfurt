/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.information.boi.adapter.delegate;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountInformationResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.constants.AccountInformationFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.transformer.AccountInformationFoundationServiceTransformer;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountInformationFoundationServiceDelegate.
 */
@EnableConfigurationProperties
@ConfigurationProperties("foundationService")
@Component
public class AccountInformationFoundationServiceDelegate {

	/** The adapterUtility. */
	@Autowired
	private AdapterUtility adapterUtility;

	private Map<String, String> foundationCustProfileUrl = new HashMap<>();

	/** The account information FS transformer. */
	@Autowired
	AccountInformationFoundationServiceTransformer accountInformationFSTransformer;

	/** The user in req header. */
	@Value("${foundationService.userInReqHeader:#{X-BOI-USER}}")
	private String userInReqHeader;

	/** The channel in req header. */
	@Value("${foundationService.channelInReqHeader:#{X-BOI-CHANNEL}}")
	private String channelInReqHeader;

	/** The platform in req header. */
	@Value("${foundationService.platformInReqHeader:#{X-BOI-PLATFORM}}")
	private String platformInReqHeader;

	/** The correlation id in req header. */
	@Value("${foundationService.correlationReqHeader:#{X-api-CORRELATION-ID}}")
	private String correlationReqHeader;

	/** The user in req header. */
	@Value("${foundationService.userInReqHeadeRaml:#{x-api-source-user}}")
	private String userInReqHeadeRaml;

	/** The platform in req header. */
	@Value("${foundationService.platformInReqHeaderRaml:#{x-api-source-system}}")
	private String platformInReqHeaderRaml;

	/** The correlation id in req header. */
	@Value("${foundationService.transactionReqHeaderRaml:#{x-api-transaction-id}}")
	private String transactionReqHeaderRaml;

	/** The correlation id in req header. */
	@Value("${foundationService.correlationReqHeaderRaml:#{x-api-correlation-id}}")
	private String correlationReqHeaderRaml;
	
	@Value("${foundationService.maskedPan:#{X-MASKED-PAN}}")
	private String maskedPan;
	
	@Value("${foundationService.channelInReqHeaderRaml:#{x-api-channel-Code}}")
	private String channelInReqHeaderRaml;

	/** The platform. */
	@Value("${app.platform}")
	private String platform;

	/**
	 * Gets the foundation service URL.
	 *
	 * @param channelId
	 *            the channel id
	 * @param accountNSC
	 *            the account NSC
	 * @param accountNumber
	 *            the account number
	 * @param version 
	 * @return the foundation service URL
	 */
	public String getFoundationServiceURL(String version,String channelId, String accountNSC, String accountNumber) {
		if (NullCheckUtils.isNullOrEmpty(channelId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_CHANNEL_ID_IN_REQUEST);
		}
		if (NullCheckUtils.isNullOrEmpty(accountNSC)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_NSC_IN_REQUEST);
		}
		if (NullCheckUtils.isNullOrEmpty(accountNumber)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		String baseURL = adapterUtility.retriveFoundationServiceURL(channelId);
		if (NullCheckUtils.isNullOrEmpty(baseURL)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		
		return baseURL + "/" + version + "/" + "accounts" + "/" + accountNSC + "/" + accountNumber;
	}

	public String getFoundationServiceURL(AccountMapping accountMapping, Map<String, String> params) {
		String finalURL = foundationCustProfileUrl
				.get((params.get(AccountInformationFoundationServiceConstants.CHANNEL_ID)).toUpperCase()) + "/"
				+ accountMapping.getPsuId() + "/" + "accounts";
		return finalURL;

	}

	/**
	 * Transform response from FD to API.
	 *
	 * @param accnt
	 *            the accnt
	 * @param params
	 *            the params
	 * @return the account GET response
	 */
	public <T> PlatformAccountInformationResponse transformResponseFromFDToAPI(T inputBalanceObj,
			Map<String, String> params) {
		PlatformAccountInformationResponse finalAIResponseObj = new PlatformAccountInformationResponse();
		if (params.get(AccountInformationFoundationServiceConstants.ACCOUNT)
				.equals(AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT)) {
			if (params.get(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE)
					.equals(AccountInformationFoundationServiceConstants.CURRENT_ACCOUNT) || params.get(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE)
					.equals(AccountInformationFoundationServiceConstants.SAVINGS)) {
				Account accounts = (Account) inputBalanceObj;
				finalAIResponseObj = accountInformationFSTransformer.transformAccountInformation(accounts, params);
			} else if (params.get(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE)
					.equals(AccountInformationFoundationServiceConstants.CREDIT_CARD)) {
				Account accounts = (Account) inputBalanceObj;
				finalAIResponseObj = accountInformationFSTransformer.transformAccountInformation(accounts, params);
			}
		} else if (params.get(AccountInformationFoundationServiceConstants.ACCOUNT)
				.equals(AccountInformationFoundationServiceConstants.MULTI_ACCOUNT)) {
			DigitalUserProfile multiAccounts = (DigitalUserProfile) inputBalanceObj;
			finalAIResponseObj = accountInformationFSTransformer.transformAccountInformation(multiAccounts, params);
		}

		return finalAIResponseObj;
	}

	/**
	 * Gets the account from account list.
	 *
	 * @param accountNumber
	 *            the account number
	 * @param accounts
	 *            the accounts
	 * @return the account from account list
	 */
	public Accnt getAccountFromAccountList(String accountNumber, Accnts accounts) {
		for (Accnt account : accounts.getAccount()) {
			if (account.getAccountNumber().equalsIgnoreCase(accountNumber)) {
				return account;
			}
		}
		return null;
	}

	/**
	 * Creates the request headers.
	 *
	 * @param requestInfo
	 *            the request info
	 * @param accountMapping
	 *            the account mapping
	 * @return the http headers
	 */
	public HttpHeaders createRequestHeaders(RequestInfo requestInfo, AccountMapping accountMapping,
			Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(userInReqHeadeRaml, accountMapping.getPsuId());
		httpHeaders.add(channelInReqHeaderRaml, params.get(AccountInformationFoundationServiceConstants.CHANNEL_ID));
		httpHeaders.add(platformInReqHeaderRaml, platform);
		httpHeaders.add(transactionReqHeaderRaml, accountMapping.getCorrelationId());
		httpHeaders.add(correlationReqHeaderRaml, "");
		return httpHeaders;
	}

	public Map<String, String> getFoundationCustProfileUrl() {
		return foundationCustProfileUrl;
	}

	public void setFoundationCustProfileUrl(Map<String, String> foundationCustProfileUrl) {
		this.foundationCustProfileUrl = foundationCustProfileUrl;
	}

	public HttpHeaders createCreditCardRequestHeaders(RequestInfo requestInfo, AccountMapping accountMapping,
			Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(userInReqHeadeRaml, accountMapping.getPsuId());
		httpHeaders.add(channelInReqHeaderRaml, params.get(AccountInformationFoundationServiceConstants.CHANNEL_ID));
		httpHeaders.add(platformInReqHeaderRaml, platform);
		httpHeaders.add(transactionReqHeaderRaml, accountMapping.getCorrelationId());
		httpHeaders.add(correlationReqHeaderRaml, "");
		httpHeaders.add(maskedPan, params.get("maskedPan"));
		return httpHeaders;
	}

	public String getCreditCardFoundationServiceURL(String version,String customerReference, String accountNumber, String channelId ) {
		
		if (NullCheckUtils.isNullOrEmpty(customerReference)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if (NullCheckUtils.isNullOrEmpty(channelId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_CHANNEL_ID_IN_REQUEST);
		}
		if (NullCheckUtils.isNullOrEmpty(accountNumber)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		String baseURL = adapterUtility.retriveFoundationServiceRamlURL(channelId);
		if (NullCheckUtils.isNullOrEmpty(baseURL)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		
		return baseURL + "/"+version+ "/" + "accounts/creditcards" + "/"+ customerReference + "/" + accountNumber;
	}

	public String getFoundationServiceRamlURL(AccountMapping accountMapping, Map<String, String> params) {
		String finalURL = foundationCustProfileUrl
				.get((params.get(AccountInformationFoundationServiceConstants.CHANNEL_ID)).toUpperCase())
				+ params.get(AccountInformationFoundationServiceConstants.CHANNEL_ID).toUpperCase() + "/digitalusers/"
				+ accountMapping.getPsuId() + "/" + "digital-user-profile";
		return finalURL;

	}
}