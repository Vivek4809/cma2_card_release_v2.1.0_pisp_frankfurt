package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.util.Objects;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * This is the counterparty (payee) account for a payment instruction
 */
@ApiModel(description = "This is the counterparty (payee) account for a payment instruction")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class PaymentInstructionCounterparty   {
  @JsonProperty("counterParty")
  private Party counterParty = null;

  @JsonProperty("counterPartyAccountBasic")
  private PaymentInstructionCounterpartyAccountBasic counterPartyAccountBasic = null;

  @JsonProperty("counterPartyAccount")
  private Account counterPartyAccount = null;

  public PaymentInstructionCounterparty counterParty(Party counterParty) {
    this.counterParty = counterParty;
    return this;
  }

  /**
   * Get counterParty
   * @return counterParty
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Party getCounterParty() {
    return counterParty;
  }

  public void setCounterParty(Party counterParty) {
    this.counterParty = counterParty;
  }

  public PaymentInstructionCounterparty counterPartyAccountBasic(PaymentInstructionCounterpartyAccountBasic counterPartyAccountBasic) {
    this.counterPartyAccountBasic = counterPartyAccountBasic;
    return this;
  }

  /**
   * Get counterPartyAccountBasic
   * @return counterPartyAccountBasic
  **/
  @ApiModelProperty(value = "")

  @Valid

  public PaymentInstructionCounterpartyAccountBasic getCounterPartyAccountBasic() {
    return counterPartyAccountBasic;
  }

  public void setCounterPartyAccountBasic(PaymentInstructionCounterpartyAccountBasic counterPartyAccountBasic) {
    this.counterPartyAccountBasic = counterPartyAccountBasic;
  }

  public PaymentInstructionCounterparty counterPartyAccount(Account counterPartyAccount) {
    this.counterPartyAccount = counterPartyAccount;
    return this;
  }

  /**
   * Get counterPartyAccount
   * @return counterPartyAccount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Account getCounterPartyAccount() {
    return counterPartyAccount;
  }

  public void setCounterPartyAccount(Account counterPartyAccount) {
    this.counterPartyAccount = counterPartyAccount;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentInstructionCounterparty paymentInstructionCounterparty = (PaymentInstructionCounterparty) o;
    return Objects.equals(this.counterParty, paymentInstructionCounterparty.counterParty) &&
        Objects.equals(this.counterPartyAccountBasic, paymentInstructionCounterparty.counterPartyAccountBasic) &&
        Objects.equals(this.counterPartyAccount, paymentInstructionCounterparty.counterPartyAccount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(counterParty, counterPartyAccountBasic, counterPartyAccount);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentInstructionCounterparty {\n");
    
    sb.append("    counterParty: ").append(toIndentedString(counterParty)).append("\n");
    sb.append("    counterPartyAccountBasic: ").append(toIndentedString(counterPartyAccountBasic)).append("\n");
    sb.append("    counterPartyAccount: ").append(toIndentedString(counterPartyAccount)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

