package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets balanceType
 */
public enum BalanceType {
  
  LEDGER_BALANCE("Ledger Balance"),
  
  SHADOW_BALANCE("Shadow Balance"),
  
  STATEMENT_CLOSING_BALANCE("Statement Closing Balance"),
  
  AVAILABLE_LIMIT("Available Limit"),
  
  CREDIT_LIMIT("Credit Limit"),
  
  MINIMUM_PAYMENT("Minimum Payment");

  private String value;

  BalanceType(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static BalanceType fromValue(String text) {
    for (BalanceType b : BalanceType.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

