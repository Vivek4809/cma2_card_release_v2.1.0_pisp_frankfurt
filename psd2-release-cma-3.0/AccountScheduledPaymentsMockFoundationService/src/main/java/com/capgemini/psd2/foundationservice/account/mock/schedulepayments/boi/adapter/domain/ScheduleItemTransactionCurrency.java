package com.capgemini.psd2.foundationservice.account.mock.schedulepayments.boi.adapter.domain;

import java.util.Objects;

/**
 * Currency object
 */
//@ApiModel(description = "Currency object")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-03T11:38:41.280+05:30")
public class ScheduleItemTransactionCurrency {
  //@SerializedName("isoAlphaCode")
  private String isoAlphaCode = null;

  //@SerializedName("currencyName")
  private String currencyName = null;

  public ScheduleItemTransactionCurrency isoAlphaCode(String isoAlphaCode) {
    this.isoAlphaCode = isoAlphaCode;
    return this;
  }

   /**
   * Get isoAlphaCode
   * @return isoAlphaCode
  **/
  //@ApiModelProperty(required = true, value = "")
  public String getIsoAlphaCode() {
    return isoAlphaCode;
  }

  public void setIsoAlphaCode(String isoAlphaCode) {
    this.isoAlphaCode = isoAlphaCode;
  }

  public ScheduleItemTransactionCurrency currencyName(String currencyName) {
    this.currencyName = currencyName;
    return this;
  }

   /**
   * Get currencyName
   * @return currencyName
  **/
  //@ApiModelProperty(value = "")
  public String getCurrencyName() {
    return currencyName;
  }

  public void setCurrencyName(String currencyName) {
    this.currencyName = currencyName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ScheduleItemTransactionCurrency scheduleItemTransactionCurrency = (ScheduleItemTransactionCurrency) o;
    return Objects.equals(this.isoAlphaCode, scheduleItemTransactionCurrency.isoAlphaCode) &&
        Objects.equals(this.currencyName, scheduleItemTransactionCurrency.currencyName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(isoAlphaCode, currencyName);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ScheduleItemTransactionCurrency {\n");
    
    sb.append("    isoAlphaCode: ").append(toIndentedString(isoAlphaCode)).append("\n");
    sb.append("    currencyName: ").append(toIndentedString(currencyName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

