package com.capgemini.psd2.payment.setup.insert.update.mock.foundationservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.capgemini.psd2.payment.setup.insert.update.mock.foundationservice.repository.PaymentSetupInsertUpdateRepository;

@SpringBootApplication
@EnableMongoRepositories(basePackages = { "com.capgemini.psd2" })
@ComponentScan(basePackages = { "com.capgemini.psd2" })
public class PaymentSetupInsertUpdateMockFoundationServiceApplication {

	@Autowired
	PaymentSetupInsertUpdateRepository paymentSetupCreateRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(PaymentSetupInsertUpdateMockFoundationServiceApplication.class, args);
	}

}
