/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.raml.domain.PaymentInstruction1;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.raml.domain.PaymentInstructionCounterPartyBasic;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.raml.domain.Schedule;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.raml.domain.StandingOrderFrequency;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.raml.domain.StandingOrderScheduleInstruction;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.raml.domain.StandingOrderScheduleInstructionsresponse;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.raml.domain.TransactionCurrency;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.transformer.AccountStandingOrderFoundationServiceTransformer;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStandingOrderFoundationServiceTransformerTest {
	@InjectMocks
	private AccountStandingOrderFoundationServiceTransformer accountStandingOrderFoundationServiceTransformer;

	@Mock
	@Qualifier("PSD2ResponseValidator")
	PSD2Validator validator;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void contextLoads() {

	}

	@Test
	public void testTransformAccountStandingOrders() throws ParseException {

		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");

		StandingOrderScheduleInstructionsresponse standingOrderScheduleInstructionsresponse = new StandingOrderScheduleInstructionsresponse();
		List<StandingOrderScheduleInstruction> standingOrdersList = new ArrayList<StandingOrderScheduleInstruction>();
		StandingOrderScheduleInstruction standingOrderScheduleInstruction = new StandingOrderScheduleInstruction();

		PaymentInstruction1 paymentInstruction1 = new PaymentInstruction1();
		paymentInstruction1.setPaymentInstructionNumber("12345");

		standingOrderScheduleInstruction.setPaymentInstruction(paymentInstruction1);

		List<PaymentInstructionCounterPartyBasic> paymentInstructionToCounterPartyBasic1 = new ArrayList<PaymentInstructionCounterPartyBasic>();
		PaymentInstructionCounterPartyBasic paymentInstructionCounterPartyBasic = new PaymentInstructionCounterPartyBasic();
		paymentInstructionCounterPartyBasic.setCounterpartyAccountNumber("123");
		paymentInstructionCounterPartyBasic.setCounterpartyName("abc");
		paymentInstructionCounterPartyBasic.setCounterpartyNationalSortCodeNSCNumber("345");
		paymentInstructionCounterPartyBasic.setInstructingPartyNarrativeText("rfd");

		paymentInstructionToCounterPartyBasic1.add(paymentInstructionCounterPartyBasic);

		standingOrderScheduleInstruction
				.setPaymentInstructionToCounterPartyBasic(paymentInstructionToCounterPartyBasic1);

		Schedule schedule = new Schedule();
		schedule.setScheduleNextDate(null);
		
		
		standingOrderScheduleInstruction.setSchedule(schedule);

		standingOrderScheduleInstruction.setStandingOrderAmount(123d);

		StandingOrderFrequency standingOrderFrequency = StandingOrderFrequency.MONTHLY;

		standingOrderScheduleInstruction.setStandingOrderFrequency(standingOrderFrequency);

		TransactionCurrency transactionCurrency = new TransactionCurrency();
		transactionCurrency.setIsoAlphaCode("cdf");

		standingOrderScheduleInstruction.setTransactionCurrency(transactionCurrency);

		standingOrdersList.add(standingOrderScheduleInstruction);

		standingOrderScheduleInstructionsresponse.setStandingOrdersList(standingOrdersList);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");

		accountStandingOrderFoundationServiceTransformer
				.transformAccountStandingOrders(standingOrderScheduleInstructionsresponse, params);
	}

	@Test
	public void testPaymentInstructionNumberIsNull() {

		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");

		StandingOrderScheduleInstructionsresponse standingOrderScheduleInstructionsresponse = new StandingOrderScheduleInstructionsresponse();
		List<StandingOrderScheduleInstruction> standingOrdersList = new ArrayList<StandingOrderScheduleInstruction>();
		StandingOrderScheduleInstruction standingOrderScheduleInstruction = new StandingOrderScheduleInstruction();

		PaymentInstruction1 paymentInstruction1 = new PaymentInstruction1();
		paymentInstruction1.setPaymentInstructionNumber(null);

		standingOrderScheduleInstruction.setPaymentInstruction(paymentInstruction1);

		List<PaymentInstructionCounterPartyBasic> paymentInstructionToCounterPartyBasic1 = new ArrayList<PaymentInstructionCounterPartyBasic>();
		PaymentInstructionCounterPartyBasic paymentInstructionCounterPartyBasic = new PaymentInstructionCounterPartyBasic();
		paymentInstructionCounterPartyBasic.setCounterpartyAccountNumber("123");
		paymentInstructionCounterPartyBasic.setCounterpartyName("abc");
		paymentInstructionCounterPartyBasic.setCounterpartyNationalSortCodeNSCNumber("345");
		paymentInstructionCounterPartyBasic.setInstructingPartyNarrativeText(null);

		paymentInstructionToCounterPartyBasic1.add(paymentInstructionCounterPartyBasic);

		standingOrderScheduleInstruction
				.setPaymentInstructionToCounterPartyBasic(paymentInstructionToCounterPartyBasic1);

		Schedule schedule = new Schedule();
		schedule.setScheduleNextDate(null);

		standingOrderScheduleInstruction.setSchedule(schedule);

		standingOrderScheduleInstruction.setStandingOrderAmount(123d);

		StandingOrderFrequency standingOrderFrequency = StandingOrderFrequency.MONTHLY;

		standingOrderScheduleInstruction.setStandingOrderFrequency(standingOrderFrequency);

		TransactionCurrency transactionCurrency = new TransactionCurrency();
		transactionCurrency.setIsoAlphaCode("cdf");

		standingOrderScheduleInstruction.setTransactionCurrency(transactionCurrency);

		standingOrdersList.add(standingOrderScheduleInstruction);

		standingOrderScheduleInstructionsresponse.setStandingOrdersList(standingOrdersList);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");

		accountStandingOrderFoundationServiceTransformer
				.transformAccountStandingOrders(standingOrderScheduleInstructionsresponse, params);

	}

	@Test
	public void testInstructingPartyNarrativeTextIsNull() {

		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");

		StandingOrderScheduleInstructionsresponse standingOrderScheduleInstructionsresponse = new StandingOrderScheduleInstructionsresponse();
		List<StandingOrderScheduleInstruction> standingOrdersList = new ArrayList<StandingOrderScheduleInstruction>();
		StandingOrderScheduleInstruction standingOrderScheduleInstruction = new StandingOrderScheduleInstruction();

		PaymentInstruction1 paymentInstruction1 = new PaymentInstruction1();
		paymentInstruction1.setPaymentInstructionNumber("12345");

		standingOrderScheduleInstruction.setPaymentInstruction(paymentInstruction1);

		List<PaymentInstructionCounterPartyBasic> paymentInstructionToCounterPartyBasic1 = new ArrayList<PaymentInstructionCounterPartyBasic>();
		PaymentInstructionCounterPartyBasic paymentInstructionCounterPartyBasic = new PaymentInstructionCounterPartyBasic();
		paymentInstructionCounterPartyBasic.setCounterpartyAccountNumber("123");
		paymentInstructionCounterPartyBasic.setCounterpartyName("abc");
		paymentInstructionCounterPartyBasic.setCounterpartyNationalSortCodeNSCNumber("345");
		paymentInstructionCounterPartyBasic.setInstructingPartyNarrativeText(null);

		paymentInstructionToCounterPartyBasic1.add(paymentInstructionCounterPartyBasic);

		standingOrderScheduleInstruction
				.setPaymentInstructionToCounterPartyBasic(paymentInstructionToCounterPartyBasic1);

		Schedule schedule = new Schedule();
		schedule.setScheduleNextDate(null);

		standingOrderScheduleInstruction.setSchedule(schedule);

		standingOrderScheduleInstruction.setStandingOrderAmount(123d);

		StandingOrderFrequency standingOrderFrequency = StandingOrderFrequency.MONTHLY;

		standingOrderScheduleInstruction.setStandingOrderFrequency(standingOrderFrequency);

		TransactionCurrency transactionCurrency = new TransactionCurrency();
		transactionCurrency.setIsoAlphaCode("cdf");

		standingOrderScheduleInstruction.setTransactionCurrency(transactionCurrency);

		standingOrdersList.add(standingOrderScheduleInstruction);

		standingOrderScheduleInstructionsresponse.setStandingOrdersList(standingOrdersList);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");

		accountStandingOrderFoundationServiceTransformer
				.transformAccountStandingOrders(standingOrderScheduleInstructionsresponse, params);

	}

	@Test
	public void testStandingOrderAmountIsNull() {

		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");

		StandingOrderScheduleInstructionsresponse standingOrderScheduleInstructionsresponse = new StandingOrderScheduleInstructionsresponse();
		List<StandingOrderScheduleInstruction> standingOrdersList = new ArrayList<StandingOrderScheduleInstruction>();
		StandingOrderScheduleInstruction standingOrderScheduleInstruction = new StandingOrderScheduleInstruction();

		PaymentInstruction1 paymentInstruction1 = new PaymentInstruction1();
		paymentInstruction1.setPaymentInstructionNumber("12345");

		standingOrderScheduleInstruction.setPaymentInstruction(paymentInstruction1);

		List<PaymentInstructionCounterPartyBasic> paymentInstructionToCounterPartyBasic1 = new ArrayList<PaymentInstructionCounterPartyBasic>();
		PaymentInstructionCounterPartyBasic paymentInstructionCounterPartyBasic = new PaymentInstructionCounterPartyBasic();
		paymentInstructionCounterPartyBasic.setCounterpartyAccountNumber("123");
		paymentInstructionCounterPartyBasic.setCounterpartyName("abc");
		paymentInstructionCounterPartyBasic.setCounterpartyNationalSortCodeNSCNumber("345");
		paymentInstructionCounterPartyBasic.setInstructingPartyNarrativeText("def");

		paymentInstructionToCounterPartyBasic1.add(paymentInstructionCounterPartyBasic);

		standingOrderScheduleInstruction
				.setPaymentInstructionToCounterPartyBasic(paymentInstructionToCounterPartyBasic1);

		Schedule schedule = new Schedule();
		schedule.setScheduleNextDate(null);

		standingOrderScheduleInstruction.setSchedule(schedule);

		standingOrderScheduleInstruction.setStandingOrderAmount(null);

		StandingOrderFrequency standingOrderFrequency = StandingOrderFrequency.MONTHLY;

		standingOrderScheduleInstruction.setStandingOrderFrequency(standingOrderFrequency);

		TransactionCurrency transactionCurrency = new TransactionCurrency();
		transactionCurrency.setIsoAlphaCode("cdf");

		standingOrderScheduleInstruction.setTransactionCurrency(transactionCurrency);

		standingOrdersList.add(standingOrderScheduleInstruction);

		standingOrderScheduleInstructionsresponse.setStandingOrdersList(standingOrdersList);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");

		accountStandingOrderFoundationServiceTransformer
				.transformAccountStandingOrders(standingOrderScheduleInstructionsresponse, params);

	}

	@Test
	public void testTransactionCurrencyIsNull() {

		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");

		StandingOrderScheduleInstructionsresponse standingOrderScheduleInstructionsresponse = new StandingOrderScheduleInstructionsresponse();
		List<StandingOrderScheduleInstruction> standingOrdersList = new ArrayList<StandingOrderScheduleInstruction>();
		StandingOrderScheduleInstruction standingOrderScheduleInstruction = new StandingOrderScheduleInstruction();

		PaymentInstruction1 paymentInstruction1 = new PaymentInstruction1();
		paymentInstruction1.setPaymentInstructionNumber("12345");

		standingOrderScheduleInstruction.setPaymentInstruction(paymentInstruction1);

		List<PaymentInstructionCounterPartyBasic> paymentInstructionToCounterPartyBasic1 = new ArrayList<PaymentInstructionCounterPartyBasic>();
		PaymentInstructionCounterPartyBasic paymentInstructionCounterPartyBasic = new PaymentInstructionCounterPartyBasic();
		paymentInstructionCounterPartyBasic.setCounterpartyAccountNumber("123");
		paymentInstructionCounterPartyBasic.setCounterpartyName("abc");
		paymentInstructionCounterPartyBasic.setCounterpartyNationalSortCodeNSCNumber("345");
		paymentInstructionCounterPartyBasic.setInstructingPartyNarrativeText("acd");

		paymentInstructionToCounterPartyBasic1.add(paymentInstructionCounterPartyBasic);

		standingOrderScheduleInstruction
				.setPaymentInstructionToCounterPartyBasic(paymentInstructionToCounterPartyBasic1);

		Schedule schedule = new Schedule();
		schedule.setScheduleNextDate(null);

		standingOrderScheduleInstruction.setSchedule(schedule);

		standingOrderScheduleInstruction.setStandingOrderAmount(123d);

		StandingOrderFrequency standingOrderFrequency = StandingOrderFrequency.MONTHLY;

		standingOrderScheduleInstruction.setStandingOrderFrequency(standingOrderFrequency);

		TransactionCurrency transactionCurrency = new TransactionCurrency();
		transactionCurrency.setIsoAlphaCode("123");

		standingOrderScheduleInstruction.setTransactionCurrency(null);

		standingOrdersList.add(standingOrderScheduleInstruction);

		standingOrderScheduleInstructionsresponse.setStandingOrdersList(standingOrdersList);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");

		accountStandingOrderFoundationServiceTransformer
				.transformAccountStandingOrders(standingOrderScheduleInstructionsresponse, params);

	}

	@Test
	public void testScheduleNextDateIsNull() {

		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");

		StandingOrderScheduleInstructionsresponse standingOrderScheduleInstructionsresponse = new StandingOrderScheduleInstructionsresponse();
		List<StandingOrderScheduleInstruction> standingOrdersList = new ArrayList<StandingOrderScheduleInstruction>();
		StandingOrderScheduleInstruction standingOrderScheduleInstruction = new StandingOrderScheduleInstruction();

		PaymentInstruction1 paymentInstruction1 = new PaymentInstruction1();
		paymentInstruction1.setPaymentInstructionNumber("12345");

		standingOrderScheduleInstruction.setPaymentInstruction(paymentInstruction1);

		List<PaymentInstructionCounterPartyBasic> paymentInstructionToCounterPartyBasic1 = new ArrayList<PaymentInstructionCounterPartyBasic>();
		PaymentInstructionCounterPartyBasic paymentInstructionCounterPartyBasic = new PaymentInstructionCounterPartyBasic();
		paymentInstructionCounterPartyBasic.setCounterpartyAccountNumber("123");
		paymentInstructionCounterPartyBasic.setCounterpartyName("abc");
		paymentInstructionCounterPartyBasic.setCounterpartyNationalSortCodeNSCNumber("345");
		paymentInstructionCounterPartyBasic.setInstructingPartyNarrativeText("acd");

		paymentInstructionToCounterPartyBasic1.add(paymentInstructionCounterPartyBasic);

		standingOrderScheduleInstruction
				.setPaymentInstructionToCounterPartyBasic(paymentInstructionToCounterPartyBasic1);

		Schedule schedule = new Schedule();
		schedule.setScheduleNextDate(null);

		standingOrderScheduleInstruction.setSchedule(schedule);

		standingOrderScheduleInstruction.setStandingOrderAmount(123d);

		StandingOrderFrequency standingOrderFrequency = StandingOrderFrequency.MONTHLY;

		standingOrderScheduleInstruction.setStandingOrderFrequency(standingOrderFrequency);

		TransactionCurrency transactionCurrency = new TransactionCurrency();
		transactionCurrency.setIsoAlphaCode("1234");

		standingOrderScheduleInstruction.setTransactionCurrency(transactionCurrency);

		standingOrdersList.add(standingOrderScheduleInstruction);

		standingOrderScheduleInstructionsresponse.setStandingOrdersList(standingOrdersList);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");

		accountStandingOrderFoundationServiceTransformer
				.transformAccountStandingOrders(standingOrderScheduleInstructionsresponse, params);

	}
}