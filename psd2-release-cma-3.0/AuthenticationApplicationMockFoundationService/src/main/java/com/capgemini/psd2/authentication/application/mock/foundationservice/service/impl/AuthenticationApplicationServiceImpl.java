package com.capgemini.psd2.authentication.application.mock.foundationservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.authentication.application.mock.foundationservice.domain.AuthenticationRequest;
import com.capgemini.psd2.authentication.application.mock.foundationservice.repository.AuthenticationApplicationRepository;
import com.capgemini.psd2.authentication.application.mock.foundationservice.service.AuthenticationApplicationService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceAuthException;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;


@Service
public class AuthenticationApplicationServiceImpl implements AuthenticationApplicationService {

	@Autowired
	private AuthenticationApplicationRepository repository;

	@Autowired
	private ValidationUtility validationUtility;

	@Override
	public AuthenticationRequest retrieveBOLUserCredentials(String userName, String password) {

		validationUtility.validateUserLogin(userName);
		AuthenticationRequest authenticationRequest = null;
		try{
			 authenticationRequest = repository.findByUserNameAndPassword(userName, password);
		}catch(Exception ex){
			throw MockFoundationServiceAuthException.populateMockFoundationServiceException(ErrorCodeEnum.INTERNAL_SERVER_ERROR);
		}
		if (authenticationRequest == null) {
			throw MockFoundationServiceAuthException.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_LOGIN_AUTH);
		}

		return authenticationRequest;
	}

	@Override
	public AuthenticationRequest retrieveB365UserCredentials(String userName, String password) {

		validationUtility.validateUserLogin(userName);
		AuthenticationRequest authenticationRequest = null;
		try{
			 authenticationRequest = repository.findByUserNameAndPassword(userName, password);
		}catch(Exception ex){
			throw MockFoundationServiceAuthException.populateMockFoundationServiceException(ErrorCodeEnum.INTERNAL_SERVER_ERROR);
		}

		if (authenticationRequest == null) {
			throw MockFoundationServiceAuthException.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_LOGIN_AUTH);
		}

		return authenticationRequest;
	}

}
