package com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain;

import java.util.Objects;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.ScheduledPaymentInstructionProposal2;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.StandingOrderInstructionProposal;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Scheduled Payment Instruction-Proposal composite object
 */
@ApiModel(description = "Scheduled Payment Instruction-Proposal composite object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-02-08T13:41:31.197+05:30")

public class StandingOrderInstructionComposite   {
  @JsonProperty("paymentInstructionProposal")
  private StandingOrderInstructionProposal paymentInstructionProposal = null;

  @JsonProperty("paymentInstruction")
  private ScheduledPaymentInstructionProposal2 paymentInstruction = null;

  public StandingOrderInstructionComposite paymentInstructionProposal(StandingOrderInstructionProposal paymentInstructionProposal) {
    this.paymentInstructionProposal = paymentInstructionProposal;
    return this;
  }

  /**
   * Get paymentInstructionProposal
   * @return paymentInstructionProposal
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public StandingOrderInstructionProposal getPaymentInstructionProposal() {
    return paymentInstructionProposal;
  }

  public void setPaymentInstructionProposal(StandingOrderInstructionProposal paymentInstructionProposal) {
    this.paymentInstructionProposal = paymentInstructionProposal;
  }

  public StandingOrderInstructionComposite paymentInstruction(ScheduledPaymentInstructionProposal2 paymentInstruction) {
    this.paymentInstruction = paymentInstruction;
    return this;
  }

  /**
   * Get paymentInstruction
   * @return paymentInstruction
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public ScheduledPaymentInstructionProposal2 getPaymentInstruction() {
    return paymentInstruction;
  }

  public void setPaymentInstruction(ScheduledPaymentInstructionProposal2 paymentInstruction) {
    this.paymentInstruction = paymentInstruction;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StandingOrderInstructionComposite standingOrderInstructionComposite = (StandingOrderInstructionComposite) o;
    return Objects.equals(this.paymentInstructionProposal, standingOrderInstructionComposite.paymentInstructionProposal) &&
        Objects.equals(this.paymentInstruction, standingOrderInstructionComposite.paymentInstruction);
  }

  @Override
  public int hashCode() {
    return Objects.hash(paymentInstructionProposal, paymentInstruction);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StandingOrderInstructionComposite {\n");
    
    sb.append("    paymentInstructionProposal: ").append(toIndentedString(paymentInstructionProposal)).append("\n");
    sb.append("    paymentInstruction: ").append(toIndentedString(paymentInstruction)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

