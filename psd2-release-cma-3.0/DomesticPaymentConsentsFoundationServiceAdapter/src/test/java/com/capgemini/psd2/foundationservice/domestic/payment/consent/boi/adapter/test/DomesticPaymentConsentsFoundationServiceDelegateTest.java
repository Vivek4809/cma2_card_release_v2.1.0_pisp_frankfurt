package com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.delegate.DomesticPaymentConsentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.PaymentInstructionProposal;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticPaymentConsentsFoundationServiceDelegateTest {
	
	@InjectMocks
	DomesticPaymentConsentsFoundationServiceDelegate delegate;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testcreatePaymentRequestHeaders(){
		ReflectionTestUtils.setField(delegate,"transactioReqHeader","transactioReqHeader");
		ReflectionTestUtils.setField(delegate,"correlationMuleReqHeader","correlation");
		ReflectionTestUtils.setField(delegate,"sourceUserReqHeader","sorceuser");
		ReflectionTestUtils.setField(delegate,"sourcesystem","source");
		ReflectionTestUtils.setField(delegate,"apiChannelCode","channel");
		ReflectionTestUtils.setField(delegate,"domesticPaymentConsentBaseURL","baseUrl");
		RequestInfo requestInfo = new RequestInfo();
		Map<String, String> params=new HashMap<>();
		CustomPaymentStageIdentifiers c=new CustomPaymentStageIdentifiers();
		params.put("transactioReqHeader", "boi");
		HttpHeaders header=delegate.createPaymentRequestHeaders(c,requestInfo, params);
		assertNotNull(header);
		
		
	}
	
	@Test
	public void testcreatePaymentRequestHeadersPost(){
		ReflectionTestUtils.setField(delegate,"apiChannelCode","apiChannelCode");
		ReflectionTestUtils.setField(delegate,"sourceUserReqHeader","sorceuser");
		ReflectionTestUtils.setField(delegate,"transactioReqHeader","transactioReqHeader");
		ReflectionTestUtils.setField(delegate,"correlationMuleReqHeader","");
		ReflectionTestUtils.setField(delegate,"apiChannelCode","channel");
		ReflectionTestUtils.setField(delegate,"sourcesystem","PSD2API");
		RequestInfo requestInfo = new RequestInfo();
		Map<String, String> params=new HashMap<>();
		CustomPaymentStageIdentifiers c=new CustomPaymentStageIdentifiers();
		params.put("transactioReqHeader", "boi");
		HttpHeaders header=delegate.createPaymentRequestHeadersPost(c,requestInfo, params);
		assertNotNull(header);
		
		
	}
	
	@Test
	public void testgetPaymentFoundationServiceURL(){
		String version="1.0";
		String PaymentInstuctionProposalId="1007";
		delegate.getPaymentFoundationServiceURL(version, PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}
	
	@Test
	public void testpostPaymentFoundationServiceURL(){
		String version="1.0";
		String PaymentInstuctionProposalId="1007";
		delegate.postPaymentFoundationServiceURL(version);
		assertNotNull(delegate);
	}
	
/*	@Test
	public void testTransformDomesticConsentResponseFromAPIToFDForInsert(){
		
		CustomDPaymentConsentsPOSTRequest paymentConsentsRequest = new CustomDPaymentConsentsPOSTRequest();
		Map<String, String> params = new HashMap<String,String>();
		delegate.transformDomesticConsentResponseFromAPIToFDForInsert(paymentConsentsRequest, params);
		assertNotNull(delegate);
	}*/
	
	/*@Test
	public void testtransformDomesticConsentResponseFromFDToAPIForInsert(){
		
		PaymentInstructionProposal paymentInstructionProposalResponse = new PaymentInstructionProposal();	
		paymentInstructionProposalResponse.setPaymentInstructionProposalId("111");
		paymentInstructionProposalResponse.setProposalCreationDatetime(new Date(12/12/2012));
		//paymentInstructionProposalResponse.setProposalStatus();
		Map<String, String> params = new HashMap<String,String>();
		delegate.transformDomesticConsentResponseFromFDToAPIForInsert(paymentInstructionProposalResponse, params);
		assertNotNull(delegate);
	}
*/

}
