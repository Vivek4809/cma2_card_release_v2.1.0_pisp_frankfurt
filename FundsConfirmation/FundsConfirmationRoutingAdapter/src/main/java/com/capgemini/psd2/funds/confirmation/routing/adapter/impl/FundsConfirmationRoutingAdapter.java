package com.capgemini.psd2.funds.confirmation.routing.adapter.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import com.capgemini.psd2.cisp.adapter.FundsConfirmationAdapter;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.funds.confirmation.routing.adapter.routing.FundsConfirmationAdapterFactory;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.NullCheckUtils;


public class FundsConfirmationRoutingAdapter implements FundsConfirmationAdapter {

	@Value("${app.defaultAdapterFundsConfirmation}")
	private String defaultAdapter;
	
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Autowired
	private FundsConfirmationAdapterFactory fundsConfirmationConsentAdapterFactory;

	@Override
	public OBFundsConfirmationResponse1 getFundsData(AccountDetails accountDetails,
			OBFundsConfirmation1 fundsConfirmationPOSTRequest, Map<String, String> params) {
		 FundsConfirmationAdapter fundsConfirmationAdapter = fundsConfirmationConsentAdapterFactory
				.getAdapterInstance(defaultAdapter);
		return fundsConfirmationAdapter.getFundsData(accountDetails, fundsConfirmationPOSTRequest,addHeaderParams(params));
	}
	
	private Map<String, String> addHeaderParams(Map<String, String> mapParam) {
		/*
		 * Below code will be used when update service will be called from
		 * payment submission API. For submission flow -> update, PSUId and
		 * ChannelName are mandatory fields for FS
		 */
		if (NullCheckUtils.isNullOrEmpty(mapParam))
			mapParam = new HashMap<>();

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			mapParam.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		mapParam.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		mapParam.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		mapParam.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return mapParam;
	}
}
