package com.capgemini.psd2.aspsp.proxy.application;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.ribbon.ServerIntrospector;
import org.springframework.cloud.netflix.ribbon.apache.RibbonLoadBalancingHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.netflix.client.RetryHandler;
import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.ILoadBalancer;

@Configuration
public class CustomHttpClientConfig {

	@Value("${javax.net.ssl.trustStore:null}")
	private String trustStoreName;
	@Value("${javax.net.ssl.trustStorePassword:null}")
	private String trustStorePassword;

	@Bean
	public RibbonLoadBalancingHttpClient ribbonLoadBalancingHttpClient(IClientConfig ribbonClientConfig,
			ServerIntrospector serverIntrospector, ILoadBalancer loadBalancer, RetryHandler retryHandler) {
		System.setProperty("javax.net.ssl.trustStorePassword",trustStorePassword );
		CustomHttpClient client =
				new CustomHttpClient(ribbonClientConfig, serverIntrospector);
		client.setLoadBalancer(loadBalancer);
		client.setRetryHandler(retryHandler);
		return client;
	}
}