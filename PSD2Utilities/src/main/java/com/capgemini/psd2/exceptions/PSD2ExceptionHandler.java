/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.exceptions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.OBErrorResponse1;

/**
 * The Class PSD2ExceptionHandler.
 */
@ControllerAdvice
public class PSD2ExceptionHandler extends ResponseEntityExceptionHandler{
	
	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;
	
	 /**
 	 * Handle invalid request.
 	 *
 	 * @param e the e
 	 * @param request the request
 	 * @return the response entity
 	 */
 	@ExceptionHandler({Exception.class })
	 protected ResponseEntity<Object> handleInvalidRequest(Exception e, WebRequest request){
 		
 		
		ErrorInfo errorInfo = null;
		HttpStatus status = null;
		String errorResponse = null;
		OBErrorResponse1 obErrorResponse1=null;
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
		if(e != null && e instanceof HttpClientErrorException) {
			errorResponse = ((HttpClientErrorException)e).getResponseBodyAsString();
			status = ((HttpClientErrorException) e).getStatusCode();
			
			errorInfo = new ErrorInfo(String.valueOf(status), errorResponse, errorResponse, status.toString());
						
		}
		else if(e != null && e instanceof PSD2Exception) {
			if(((PSD2Exception)e).getOBErrorResponse1()!=null){
				obErrorResponse1=((PSD2Exception) e).getOBErrorResponse1();
				
				status=obErrorResponse1.getHttpStatus();
				
				if(status.is5xxServerError())
					obErrorResponse1.getErrors().get(0).setMessage(OBPSD2ExceptionUtility.specificErrorMessages.get(ErrorMapKeys.UNEXPECTED_ERROR));
					
				obErrorResponse1.setId(requestHeaderAttributes.getCorrelationId());
			}
			else {
				errorInfo = ((PSD2Exception)e).getErrorInfo();
				status = HttpStatus.valueOf(Integer.parseInt(errorInfo.getStatusCode()));
			}
		}

        if(errorInfo == null && obErrorResponse1==null){
        	status = HttpStatus.INTERNAL_SERVER_ERROR;
        	
			errorInfo = new ErrorInfo(String.valueOf(status), errorResponse, errorResponse, status.toString());
			
		}
        if(obErrorResponse1!=null)
        	return handleExceptionInternal(e, obErrorResponse1, headers, status, request);
        else
        	return handleExceptionInternal(e, errorInfo, headers, status, request);
	 }
}
