package com.capgemini.psd2.token;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class TokenAdditionalInfo {
	
	private TppInformationTokenData tppInformation;
	private ConsentTokenData consentTokenData;
	private String consentType;
	private String requestId;
	private Set<String> scope;
	private Map<String,List<Object>> claims;
	private Map<String,String> seviceParams;

	
	public TppInformationTokenData getTppInformation() {
		return tppInformation;
	}
	public void setTppInformation(TppInformationTokenData tppInformation) {
		this.tppInformation = tppInformation;
	}
	public ConsentTokenData getConsentTokenData() {
		return consentTokenData;
	}
	public void setConsentTokenData(ConsentTokenData consentTokenData) {
		this.consentTokenData = consentTokenData;
	}
	public String getConsentType() {
		return consentType;
	}
	public void setConsentType(String consentType) {
		this.consentType = consentType;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public Set<String> getScope() {
		return scope;
	}
	public void setScope(Set<String> scope) {
		this.scope = scope;
	}
	public Map<String, List<Object>> getClaims() {
		return claims;
	}
	public void setClaims(Map<String, List<Object>> claims) {
		this.claims = claims;
	}
	public Map<String, String> getSeviceParams() {
		return seviceParams;
	}
	public void setSeviceParams(Map<String, String> seviceParams) {
		this.seviceParams = seviceParams;
	}
}