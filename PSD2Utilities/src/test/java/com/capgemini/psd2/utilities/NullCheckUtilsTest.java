/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.utilities;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * The Class NullCheckUtilsTest.
 */
public class NullCheckUtilsTest {

	/**
	 * Test null check utils null obj.
	 */
	@Test
	public void testNullCheckUtilsNullObj() {
		boolean isEmpty = Boolean.TRUE;
		assertEquals(isEmpty, NullCheckUtils.isNullOrEmpty(null));
	}

	/**
	 * Test null check utils instance of string.
	 */
	@Test
	public void testNullCheckUtilsInstanceOfString() {
		boolean isEmpty = Boolean.TRUE;
		String obj = "      ";
		assertEquals(isEmpty, NullCheckUtils.isNullOrEmpty(obj));
	}
}
