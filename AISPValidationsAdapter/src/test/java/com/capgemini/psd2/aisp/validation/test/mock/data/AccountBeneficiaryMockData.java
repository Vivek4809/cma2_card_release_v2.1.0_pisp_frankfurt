package com.capgemini.psd2.aisp.validation.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBBeneficiary2;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification3;
import com.capgemini.psd2.aisp.domain.OBCashAccount3;
import com.capgemini.psd2.aisp.domain.OBPostalAddress6;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary2;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary2Data;

public class AccountBeneficiaryMockData {

	
	public static OBReadBeneficiary2 getMockExpectedAccountBeneficiaryResponse() {
		OBReadBeneficiary2 obReadBeneficiary2 = new OBReadBeneficiary2();
		OBReadBeneficiary2Data obReadBeneficiary2Data = new OBReadBeneficiary2Data();
		List<OBBeneficiary2> obBeneficiaryList = new ArrayList<>();
		OBBeneficiary2 obBeneficiary2 = new OBBeneficiary2();
		obBeneficiary2.setAccountId("123");
		obBeneficiary2.setBeneficiaryId("TestBeneficiaryId");
	
		//Setting Creditor Agent
		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		OBPostalAddress6 postalAddress = new OBPostalAddress6();
		creditorAgent.setSchemeName("BICFI");
		creditorAgent.setIdentification("Testing Scheme");
		creditorAgent.setName("Testing Creditor Agent Block");
		creditorAgent.setPostalAddress(postalAddress);
		
		//Setting Creditor Account
		OBCashAccount3 creditorAccount = new OBCashAccount3();
		creditorAccount.setSchemeName("IBAN");
		creditorAccount.setIdentification("GB29NWBK60161331926819");
		creditorAccount.setName("Creditor Account");
		creditorAccount.setSecondaryIdentification("123");
		
		obBeneficiary2.setCreditorAgent(creditorAgent);
		obBeneficiary2.setCreditorAccount(creditorAccount);
		
		
		obBeneficiaryList.add(obBeneficiary2);
		obReadBeneficiary2Data.setBeneficiary(obBeneficiaryList);

		obReadBeneficiary2.setData(obReadBeneficiary2Data);

		Links links = new Links();
		obReadBeneficiary2.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);
		obReadBeneficiary2.setMeta(meta);

		return obReadBeneficiary2;
		
	}
	
	public static OBReadBeneficiary2 getMockExpectedAccountBeneficiaryNullResponse() {
		OBReadBeneficiary2 obReadBeneficiary2 = new OBReadBeneficiary2();
		OBReadBeneficiary2Data obReadBeneficiary2Data = new OBReadBeneficiary2Data();
		List<OBBeneficiary2> obBeneficiaryList = new ArrayList<>();
		OBBeneficiary2 obBeneficiary2 = new OBBeneficiary2();
		obBeneficiary2.setAccountId("123");
		obBeneficiary2.setBeneficiaryId("TestBeneficiaryId");

		
		obBeneficiaryList.add(null);
		obReadBeneficiary2Data.setBeneficiary(obBeneficiaryList);

		obReadBeneficiary2.setData(obReadBeneficiary2Data);

		Links links = new Links();
		obReadBeneficiary2.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);
		obReadBeneficiary2.setMeta(meta);

		return obReadBeneficiary2;
		
	}

	public static OBReadBeneficiary2 getMockExpectedAccountBeneficiaryResponseWithoutCreditorAccount() {
		OBReadBeneficiary2 obReadBeneficiary2 = new OBReadBeneficiary2();
		OBReadBeneficiary2Data obReadBeneficiary2Data = new OBReadBeneficiary2Data();
		List<OBBeneficiary2> obBeneficiaryList = new ArrayList<>();
		OBBeneficiary2 obBeneficiary2 = new OBBeneficiary2();
		obBeneficiary2.setAccountId("123");
		obBeneficiary2.setBeneficiaryId("TestBeneficiaryId");
	
		//Setting Creditor Agent
		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		OBPostalAddress6 postalAddress = new OBPostalAddress6();
		creditorAgent.setSchemeName("BICFI");
		creditorAgent.setIdentification("Testing Scheme");
		creditorAgent.setName("Testing Creditor Agent Block");
		creditorAgent.setPostalAddress(postalAddress);
		obBeneficiary2.setCreditorAgent(creditorAgent);

		obBeneficiaryList.add(obBeneficiary2);
		obReadBeneficiary2Data.setBeneficiary(obBeneficiaryList);

		obReadBeneficiary2.setData(obReadBeneficiary2Data);

		Links links = new Links();
		obReadBeneficiary2.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);
		obReadBeneficiary2.setMeta(meta);

		return obReadBeneficiary2;
		
	}
	
}
