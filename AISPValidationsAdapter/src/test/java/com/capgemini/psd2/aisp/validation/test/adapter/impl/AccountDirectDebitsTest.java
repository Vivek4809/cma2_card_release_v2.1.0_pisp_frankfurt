package com.capgemini.psd2.aisp.validation.test.adapter.impl;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount;
import com.capgemini.psd2.aisp.domain.OBDirectDebit1;
import com.capgemini.psd2.aisp.domain.OBReadDirectDebit1;
import com.capgemini.psd2.aisp.domain.OBReadDirectDebit1Data;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountDirectDebitsValidatorImpl;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountDirectDebitsTest {

	
	@InjectMocks
	AccountDirectDebitsValidatorImpl accountDirectDebitsValidatorImpl;
	@Mock
	private CommonAccountValidations commonAccountValidations;
	@Mock
	private PSD2Validator psd2Validator;
	
	public  OBReadDirectDebit1 getAccountDirectDebitGETResponse()
	{

		OBReadDirectDebit1 accountGETResponse1 = new OBReadDirectDebit1();
		OBReadDirectDebit1Data data = new OBReadDirectDebit1Data();
		OBDirectDebit1 obDirectDebit1= new OBDirectDebit1();
		obDirectDebit1.setAccountId("asfda");
		obDirectDebit1.setDirectDebitId("asdf");
		obDirectDebit1.setMandateIdentification("asdfasd");
		obDirectDebit1.setName("asdfasdf");
		obDirectDebit1.setPreviousPaymentDateTime("2017-04-05T10:43:07+00:00");
		OBActiveOrHistoricCurrencyAndAmount previousPaymentAmount= new OBActiveOrHistoricCurrencyAndAmount();
		previousPaymentAmount.setAmount("asfd");
		previousPaymentAmount.setCurrency("asfd");
		obDirectDebit1.setPreviousPaymentAmount(previousPaymentAmount);
		List<OBDirectDebit1> directDebit = new ArrayList<>();
		directDebit.add(obDirectDebit1);
		data.setDirectDebit(directDebit);
		accountGETResponse1.setData(data);
		return accountGETResponse1;
	
	}
	
	@Test
	public void validateResponseParamsTest() {
		Field reqValidationEnabled = null;
			try {
				reqValidationEnabled = AccountDirectDebitsValidatorImpl.class.getDeclaredField("resValidationEnabled");
				reqValidationEnabled.setAccessible(true);
				reqValidationEnabled.set(accountDirectDebitsValidatorImpl, true);
			} catch (NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			assertTrue(accountDirectDebitsValidatorImpl.validateResponseParams(getAccountDirectDebitGETResponse()));
	}
	@Test
	public void validateResponseParamsNullParamTest() {
		Field reqValidationEnabled = null;
			try {
				reqValidationEnabled = AccountDirectDebitsValidatorImpl.class.getDeclaredField("resValidationEnabled");
				reqValidationEnabled.setAccessible(true);
				reqValidationEnabled.set(accountDirectDebitsValidatorImpl, true);
			} catch (NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			assertTrue(accountDirectDebitsValidatorImpl.validateResponseParams(null));
	}
	@Test
	public void validateResponseParamsResValidationEnabledFalseTest() {
		Field reqValidationEnabled = null;
			try {
				reqValidationEnabled = AccountDirectDebitsValidatorImpl.class.getDeclaredField("resValidationEnabled");
				reqValidationEnabled.setAccessible(true);
				reqValidationEnabled.set(accountDirectDebitsValidatorImpl, false);
			} catch (NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			assertTrue(accountDirectDebitsValidatorImpl.validateResponseParams(getAccountDirectDebitGETResponse()));
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateUniqueIdTestForNullParam()
	{
		accountDirectDebitsValidatorImpl.validateUniqueId(null);
	}
	@Test
	public void validateUniqueIdTest()
	{
		Mockito.doNothing().when(commonAccountValidations).validateUniqueUUID("asd");  
		accountDirectDebitsValidatorImpl.validateUniqueId("asdf");
	}
	@Test
	public void validateRequestParamsTest()
	{
		assertNull(accountDirectDebitsValidatorImpl.validateRequestParams(null));
	}
	
	
}
