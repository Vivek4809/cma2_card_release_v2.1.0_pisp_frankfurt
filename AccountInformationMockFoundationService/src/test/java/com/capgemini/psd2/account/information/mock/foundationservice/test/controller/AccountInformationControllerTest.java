/*package com.capgemini.psd2.account.information.mock.foundationservice.test.controller;


import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.capgemini.psd2.account.balance.mock.foundationservice.controller.AccountBalanceController;
import com.capgemini.psd2.account.balance.mock.foundationservice.domain.Account;
import com.capgemini.psd2.account.balance.mock.foundationservice.service.AccountBalanceService;
import com.capgemini.psd2.account.balance.mock.foundationservice.test.AccountBalanceApplicationTest;


@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(AccountBalanceController.class)
@SpringApplicationConfiguration(classes = AccountBalanceApplicationTest.class)

public class TestAccountInformationController {

	@Autowired
	private MockMvc mockMvc;
	

	@MockBean
	private AccountBalanceService accountBalanceService;

	Account account = null;

	@Test
	public void testReteriveAccountBalance() throws Exception {

		BigDecimal balance = new BigDecimal(55.55);
		BigDecimal postBalance = new BigDecimal(77.77);

		account = new Account();
		account.setNsc("909767");
		account.setAccountNumber("909767");
		account.setBalance(balance);
		account.setPostedBalance(postBalance);

		when(accountBalanceService.reteriveAccountBalance(account.getNsc(), account.getAccountNumber()))
				.thenReturn(account);

		this.mockMvc
				.perform(get("/psd2-abt-service/services/abt//accounts/{nsc}/{accountNumber}", account.getNsc(),
						account.getAccountNumber()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_XML))
				.andExpect(xpath("/account/balance").number(55.55))
				.andExpect(xpath("/account/postBalance").number(77.77));

	}
}
*/