package com.capgemini.psd2.pisp.payment.setup.comparator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class InternationalPaymentSetupPayloadComparator implements Comparator<OBWriteInternationalConsentResponse1> {

	@Override
	public int compare(OBWriteInternationalConsentResponse1 response,
			OBWriteInternationalConsentResponse1 adaptedPaymentSetupResponse) {

		int value = 1;

		if (Double.compare(Double.parseDouble(response.getData().getInitiation().getInstructedAmount().getAmount()) , Double
				.parseDouble(adaptedPaymentSetupResponse.getData().getInitiation().getInstructedAmount().getAmount()))==0) {
			adaptedPaymentSetupResponse.getData().getInitiation().getInstructedAmount()
					.setAmount(response.getData().getInitiation().getInstructedAmount().getAmount());
		}

		if (response.getData().getInitiation().equals(adaptedPaymentSetupResponse.getData().getInitiation())
				&& response.getRisk().equals(adaptedPaymentSetupResponse.getRisk()))
			value = 0;

		OBAuthorisation1 responseAuth = response.getData().getAuthorisation();
		OBAuthorisation1 adaptedResponseAuth = adaptedPaymentSetupResponse.getData().getAuthorisation();

		if (!NullCheckUtils.isNullOrEmpty(responseAuth) && !NullCheckUtils.isNullOrEmpty(adaptedResponseAuth)
				&& value != 1) {
			if (responseAuth.equals(adaptedResponseAuth))
				value = 0;
			else
				value = 1;
		}

		return value;
	}

	public int comparePaymentDetails(CustomIPaymentConsentsPOSTResponse response,
			CustomIPaymentConsentsPOSTRequest request, PaymentConsentsPlatformResource paymentSetupPlatformResource) {

		String strPaymentSetupRequest = JSONUtilities.getJSONOutPutFromObject(request);
		CustomIPaymentConsentsPOSTResponse adaptedPaymentSetupResponse = JSONUtilities.getObjectFromJSONString(
				PispUtilities.getObjectMapper(), strPaymentSetupRequest, CustomIPaymentConsentsPOSTResponse.class);

		if (!validateDebtorDetails(response.getData().getInitiation(),
				adaptedPaymentSetupResponse.getData().getInitiation(), paymentSetupPlatformResource))
			return 1;

		compareAmount(response.getData().getInitiation().getInstructedAmount(),
				adaptedPaymentSetupResponse.getData().getInitiation().getInstructedAmount());

		checkAndModifyEmptyFields(adaptedPaymentSetupResponse, response);

		if (!NullCheckUtils.isNullOrEmpty(response.getData().getAuthorisation())
				&& (NullCheckUtils.isNullOrEmpty(adaptedPaymentSetupResponse.getData().getAuthorisation()))) {
			return 1;
		}

		if (NullCheckUtils.isNullOrEmpty(response.getData().getAuthorisation())
				&& !NullCheckUtils.isNullOrEmpty(adaptedPaymentSetupResponse.getData().getAuthorisation())) {
			return 1;
		}

		int returnValue = compare(response, adaptedPaymentSetupResponse);

		response.getData().getInitiation()
				.setRemittanceInformation(request.getData().getInitiation().getRemittanceInformation());
		response.getRisk().setDeliveryAddress(request.getRisk().getDeliveryAddress());

		// End of CR-10
		return returnValue;

	}

	private void checkAndModifyEmptyFields(CustomIPaymentConsentsPOSTResponse adaptedPaymentSetupResponse,
			CustomIPaymentConsentsPOSTResponse response) {

		checkAndModifyRemittanceInformation(adaptedPaymentSetupResponse.getData().getInitiation(), response);

		if (adaptedPaymentSetupResponse.getRisk().getDeliveryAddress() != null) {
			checkAndModifyAddressLine(adaptedPaymentSetupResponse.getRisk().getDeliveryAddress(), response);
		}

		if (adaptedPaymentSetupResponse.getData().getInitiation().getCreditor()!=null && adaptedPaymentSetupResponse.getData().getInitiation().getCreditor().getPostalAddress() != null
				&& adaptedPaymentSetupResponse.getData().getInitiation().getCreditor().getPostalAddress()
						.getAddressLine() != null) {
			checkAndModifyCreditorPostalAddressLine(
					adaptedPaymentSetupResponse.getData().getInitiation().getCreditor().getPostalAddress(), response);
		}

	}

	private void checkAndModifyRemittanceInformation(OBInternational1 obInternational1,
			CustomIPaymentConsentsPOSTResponse response) {

		OBRemittanceInformation1 remittanceInformation = obInternational1.getRemittanceInformation();
		if (remittanceInformation != null && response.getData().getInitiation().getRemittanceInformation() == null
				&& NullCheckUtils.isNullOrEmpty(remittanceInformation.getReference())
				&& NullCheckUtils.isNullOrEmpty(remittanceInformation.getUnstructured())) {

			obInternational1.setRemittanceInformation(null);
		}
	}

	private void checkAndModifyAddressLine(OBRisk1DeliveryAddress riskDeliveryAddress,
			CustomIPaymentConsentsPOSTResponse response) {

		if (riskDeliveryAddress.getAddressLine() != null) {
			List<String> addressLineList = new ArrayList<>();
			for (String addressLine : riskDeliveryAddress.getAddressLine()) {
				if (NullCheckUtils.isNullOrEmpty(addressLine))
					continue;
				addressLineList.add(addressLine);
			}
			if (addressLineList.isEmpty() && response.getRisk().getDeliveryAddress().getAddressLine() == null)
				addressLineList = null;

			riskDeliveryAddress.setAddressLine(addressLineList);

		}
	}

	private void checkAndModifyCreditorPostalAddressLine(OBPostalAddress6 obPostalAddress6,
			CustomIPaymentConsentsPOSTResponse response) {
		List<String> addressLineList = new ArrayList<>();
		for (String addressLine : (obPostalAddress6.getAddressLine())) {
			if (NullCheckUtils.isNullOrEmpty(addressLine))
				continue;
			addressLineList.add(addressLine);
		}
		if (addressLineList.isEmpty()
				&& response.getData().getInitiation().getCreditor().getPostalAddress().getAddressLine() == null)
			addressLineList = null;

		obPostalAddress6.setAddressLine(addressLineList);
	}

	private boolean validateDebtorDetails(OBInternational1 obInternational1, OBInternational1 obInternational12,
			PaymentConsentsPlatformResource paymentSetupPlatformResource) {

		if (Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())) {

			/*
			 * If DebtorAccount.Name is sent by TPP,tppDebtoNameDetails would be true, then
			 * name will participate in comparison else product will set foundation response
			 * DebtorAccount.Name as null so that it can be passed in comparison. Both
			 * fields would have the value as 'Null'.
			 */

			if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails()))
				obInternational1.getDebtorAccount().setName(null);

			return Objects.equals(obInternational1.getDebtorAccount(), obInternational12.getDebtorAccount());
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& obInternational12.getDebtorAccount() != null) {
			return Boolean.FALSE;
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& obInternational1.getDebtorAccount() != null) {
			obInternational1.setDebtorAccount(null);
			return Boolean.TRUE;
		}
		return Boolean.TRUE;

	}

	/*
	 * Date: 12-Jan-2018, SIT defect#956 Comparing two float values received from FS
	 * and TPP. Both values must be treated equal mathematically if values are in
	 * below format. i.e 1.2 == 1.20, 0.20000 == 0.20, 0.2 == 00.20000, 0.2 ==
	 * 0.20000, 001.00 == 1.00 etc. If values are equal then set the amount received
	 * from TPP to the amount object of FS response. By doing this, comparison would
	 * be passed in swagger's java file(compare method of this class) and same
	 * amount would be available to TPP also in response of idempotent request.
	 */

	private void compareAmount(OBDomestic1InstructedAmount obActiveOrHistoricCurrencyAndAmount,
			OBDomestic1InstructedAmount obActiveOrHistoricCurrencyAndAmount2) {
		if (Double.compare(Double.parseDouble(obActiveOrHistoricCurrencyAndAmount.getAmount()),
				Double.parseDouble(obActiveOrHistoricCurrencyAndAmount2.getAmount())) == 0)
			obActiveOrHistoricCurrencyAndAmount.setAmount(obActiveOrHistoricCurrencyAndAmount2.getAmount());
	}

}
