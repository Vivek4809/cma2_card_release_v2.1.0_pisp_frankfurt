package com.capgemini.psd2.security.saas.constants;

public class SaaSAppConstants {

	public static final String DIGITAL_USER = "digitalUser";
	
	public static final String CUSTOMER_AUTHENTICATION_SESSIONS = "customerAuthenticationSessions";
	
	public static final String ELECTRONIC_DEVICES = "electronicDevices";
	
	public static final String DIGITAL_USER_SESSION = "digitalUserSession";
	
	public static final String SESSION_INITIATION_FAILURE_INDICATOR = "sessionInitiationFailureIndicator";
}
