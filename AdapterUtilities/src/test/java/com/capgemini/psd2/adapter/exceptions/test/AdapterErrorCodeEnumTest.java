/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.adapter.exceptions.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;

/**
 * The Class AdapterErrorCodeEnumTest.
 */
public class AdapterErrorCodeEnumTest {
	
	/** The adapter error code enum. */
	private AdapterErrorCodeEnum adapterErrorCodeEnum;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		adapterErrorCodeEnum = AdapterErrorCodeEnum.ACCOUNT_DETAILS_NOT_FOUND;
	}
	
	/**
	 * Test.
	 */
	@Test
	public void test(){
		assertEquals(HttpStatus.BAD_GATEWAY.toString(), adapterErrorCodeEnum.getErrorCode()); 
		assertEquals("No account details found for the requested account(s)", adapterErrorCodeEnum.getErrorMessage()); 
		assertEquals(HttpStatus.BAD_REQUEST.toString(), adapterErrorCodeEnum.getStatusCode()); 
		
	}
	
}
