package com.capgemini.psd2.adapter.frequency.utility.test;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.frequency.utility.FrequencyUtil;


@RunWith(SpringJUnit4ClassRunner.class)
public class FrequencyUtilTest {
	
	@InjectMocks
	private FrequencyUtil frequencyUtil;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testCmaToProcessLayer(){
		String freq="IntrvlWkDay:01:05";
		String frequency="";
		Date paymentStartDate = new Date("03/08/2019");
		FrequencyUtil frequencyUtil =new FrequencyUtil();
		frequency = frequencyUtil.CmaToProcessLayer(freq, paymentStartDate);
		String finalFrequency = frequencyUtil.processLayerToCma(frequency, paymentStartDate);
		assertEquals(finalFrequency,"IntrvlWkDay:01:05");
	}
	
	@Test(expected = AdapterException.class)
	public void testCmaToProcessLayer1(){
		String freq="IntrvlWkDay:01:05";
		String frequency="";
		Date paymentStartDate = new Date("03/07/2019");
		frequency = frequencyUtil.CmaToProcessLayer(freq, paymentStartDate);
		String finalFrequency = frequencyUtil.processLayerToCma(freq, paymentStartDate);
		assertEquals("","");
	}
	
	@Test(expected = AdapterException.class)
	public void testCmaToProcessLayer2(){
		String freq="";
		String frequency="";
		Date paymentStartDate = new Date("03/07/2019");
		frequency = frequencyUtil.CmaToProcessLayer(freq, paymentStartDate);
		String finalFrequency = frequencyUtil.processLayerToCma(freq, paymentStartDate);
		assertEquals(finalFrequency,"IntrvlWkDay:01:05");
	}
	
	@Test(expected = AdapterException.class)
	public void testCmaToProcessLayer0(){
		String freq="IntrvlWkDay:01:05";
		String frequency="";
		Date paymentStartDate = null;
		FrequencyUtil frequencyUtil =new FrequencyUtil();
		frequency = frequencyUtil.CmaToProcessLayer(freq, paymentStartDate);
		String finalFrequency = frequencyUtil.processLayerToCma(frequency, paymentStartDate);
		assertEquals(finalFrequency,"IntrvlWkDay:01:05");
	}
	
	@Test
	public void testCmaToProcessLayer3(){
		String freq="IntrvlWkDay:02:05";
		String frequency="";
		Date paymentStartDate = new Date("03/08/2019");
		frequency = frequencyUtil.CmaToProcessLayer(freq, paymentStartDate);
		String finalFrequency = frequencyUtil.processLayerToCma(frequency, paymentStartDate);
		assertEquals(finalFrequency,"IntrvlWkDay:02:05");
	}
	
	@Test
	public void testCmaToProcessLayer4(){
		String freq="IntrvlMnthDay:01:21";
		String frequency="";
		Date paymentStartDate = new Date("03/21/2019");
		frequency = frequencyUtil.CmaToProcessLayer(freq, paymentStartDate);
		String finalFrequency = frequencyUtil.processLayerToCma(frequency, paymentStartDate);
		assertEquals(finalFrequency,"IntrvlMnthDay:01:21");
	}
	
	@Test
	public void testCmaToProcessLayer5(){		
		String freq="IntrvlMnthDay:12:21";
		String frequency="";
		Date paymentStartDate = new Date("03/21/2019");
		frequency = frequencyUtil.CmaToProcessLayer(freq, paymentStartDate);
		String finalFrequency = frequencyUtil.processLayerToCma(frequency, paymentStartDate);
		assertEquals(frequency,"Yearly");
		assertEquals(finalFrequency,"IntrvlMnthDay:12:21");		
	}
	
	
}
