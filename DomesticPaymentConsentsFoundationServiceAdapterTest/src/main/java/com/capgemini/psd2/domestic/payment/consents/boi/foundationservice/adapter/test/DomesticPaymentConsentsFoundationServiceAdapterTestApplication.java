package com.capgemini.psd2.domestic.payment.consents.boi.foundationservice.adapter.test;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.DomesticPaymentConsentsFoundationServiceAdapter;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class DomesticPaymentConsentsFoundationServiceAdapterTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(DomesticPaymentConsentsFoundationServiceAdapterTestApplication.class, args);
	}
}

@RestController
@ResponseBody
class TestCustomerAccountProfileController
{
	
	@Autowired
	private DomesticPaymentConsentsFoundationServiceAdapter domesticPaymentConsentsFoundationServiceAdapter;
	
	@RequestMapping("/testDomesticPayment")
	public CustomDPaymentConsentsPOSTResponse retrieveDomesticPayment()
	{
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers= new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId("1234567");
		customPaymentStageIdentifiers.setPaymentSetupVersion("1.0");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		Map<String,String> params=new HashMap<String ,String>();
	    params.put("consentFlowType", "AISP");
	    params.put("x-channel-id", "BOL");
	    params.put("x-user-id", "BOI999");
	    params.put("X-BOI-PLATFORM", "platform");
	    params.put("x-fapi-interaction-id", "87654321");
		
		return domesticPaymentConsentsFoundationServiceAdapter.retrieveStagedDomesticPaymentConsents(customPaymentStageIdentifiers, params);
	}
	

}