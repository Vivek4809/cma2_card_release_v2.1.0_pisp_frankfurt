package com.capgemini.psd2.integration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.integration.service.TPPInformationService;
import com.capgemini.psd2.tppinformation.adaptor.ldap.constants.TPPInformationConstants;

@RestController
public class JWKController {

	@Autowired
	private TPPInformationService service;
	
	@RequestMapping(value = "/{orgId}/{clientid}.jwks", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public String publicKeyForClient(@PathVariable ("orgId") String orgId,
			@PathVariable ("clientid") String clientId,@RequestParam(value="tenantId",required = false) String tenantId)
	{
		return service.fetchClientInformation(clientId,TPPInformationConstants.PUBLIC_KEY);
	}
	
	@RequestMapping(value="/getJwksUrl/{clientId}", method = RequestMethod.GET)
	public String fetchJwksUrlForFiles(@PathVariable("clientId") String clientId) {
		return service.fetchJWKSUrl(clientId);
	}
}
