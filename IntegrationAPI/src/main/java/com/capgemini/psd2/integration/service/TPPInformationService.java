package com.capgemini.psd2.integration.service;

import com.capgemini.psd2.integration.dtos.TPPInformationDTO;

public interface TPPInformationService {

	public TPPInformationDTO findTPPInformation(String clientId);
	
	public String fetchClientInformation(String clientId, String attrbiute);
	
	public String fetchJWKSUrl(String clientId);
	
}
