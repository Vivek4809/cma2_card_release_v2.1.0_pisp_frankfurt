package com.capgemini.psd2.mock.foundationservice.account.transactions.exception;

public class InvalidParameterRequestException extends Exception {

	public InvalidParameterRequestException(String i) {
		super(i);

	}

}
