package com.capgemini.psd2.sca.authentication.retrieve.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.sca.authentication.retrieve.mock.foundationservice.domain.AuthenticationParameters;


public interface SCAAuthenticationRetrieveServiceRepository extends MongoRepository<AuthenticationParameters , String > {

	public AuthenticationParameters findAuthenticationParametersByDigitalUserDigitalUserIdentifier(String digitalUserId);
	

}

