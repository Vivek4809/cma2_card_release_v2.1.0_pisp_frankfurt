package com.capgemini.psd2.account.beneficiaries.mongo.db.adapter.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountBeneficiaryCMA2;

public interface AccountBeneficiaryRepository extends MongoRepository<AccountBeneficiaryCMA2, String>{
	
	
	/**
	 * Find by account id.
	 *
	 * @param accountId the account id
	 * @return the beneficiary GET response data
	 */
	public List<AccountBeneficiaryCMA2> findByAccountNumberAndAccountNSC (String accountNumber, String accountNSC);

}
