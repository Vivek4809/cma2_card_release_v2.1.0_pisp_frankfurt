package com.capgemini.tpp.registration.model;
/**
 * Ping Federate Error Model
 * @author nagoswam
 *
 */
public class PfErrorModel {

	private String resultId;

	private String message;

	private PfValidationError[] validationErrors;

	public String getResultId() {
		return resultId;
	}

	public void setResultId(String resultId) {
		this.resultId = resultId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public PfValidationError[] getValidationErrors() {
		return validationErrors;
	}

	public void setValidationErrors(PfValidationError[] validationErrors) {
		this.validationErrors = validationErrors;
	}
}
