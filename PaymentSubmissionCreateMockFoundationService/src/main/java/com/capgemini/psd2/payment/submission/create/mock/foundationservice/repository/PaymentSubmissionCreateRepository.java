package com.capgemini.psd2.payment.submission.create.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.payment.submission.create.mock.foundationservice.domain.PaymentInstruction;

public interface PaymentSubmissionCreateRepository extends MongoRepository<PaymentInstruction, String>{

	public PaymentInstruction findByPaymentPaymentId(String PaymentId);
}
