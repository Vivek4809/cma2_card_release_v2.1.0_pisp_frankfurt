package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.time.OffsetDateTime;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModelProperty;

/**
 * OnlineUser
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OnlineUser {
  @SerializedName("userIdentifier")
  private String userIdentifier;

  @SerializedName("userEnrollmentDate")
  private OffsetDateTime userEnrollmentDate;

  public OnlineUser userIdentifier(String userIdentifier) {
    this.userIdentifier = userIdentifier;
    return this;
  }

   /**
   * Get userIdentifier
   * @return userIdentifier
  **/
  @ApiModelProperty(required = true, value = "")
  public String getUserIdentifier() {
    return userIdentifier;
  }

  public void setUserIdentifier(String userIdentifier) {
    this.userIdentifier = userIdentifier;
  }

  public OnlineUser userEnrollmentDate(OffsetDateTime userEnrollmentDate) {
    this.userEnrollmentDate = userEnrollmentDate;
    return this;
  }

   /**
   * Get userEnrollmentDate
   * @return userEnrollmentDate
  **/
  @ApiModelProperty(value = "")
  public OffsetDateTime getUserEnrollmentDate() {
    return userEnrollmentDate;
  }

  public void setUserEnrollmentDate(OffsetDateTime userEnrollmentDate) {
    this.userEnrollmentDate = userEnrollmentDate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OnlineUser onlineUser = (OnlineUser) o;
    return Objects.equals(this.userIdentifier, onlineUser.userIdentifier) &&
        Objects.equals(this.userEnrollmentDate, onlineUser.userEnrollmentDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userIdentifier, userEnrollmentDate);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OnlineUser {\n");
    
    sb.append("    userIdentifier: ").append(toIndentedString(userIdentifier)).append("\n");
    sb.append("    userEnrollmentDate: ").append(toIndentedString(userEnrollmentDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

