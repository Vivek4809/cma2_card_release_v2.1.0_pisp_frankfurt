package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModelProperty;

/**
 * ErrorInfo
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorInfo {
	@SerializedName("replayCount")
	private Integer replayCount = null;

	@SerializedName("lastError")
	private String lastError = null;

	public ErrorInfo replayCount(Integer replayCount) {
		this.replayCount = replayCount;
		return this;
	}

	/**
	 * Get replayCount
	 * 
	 * @return replayCount
	 **/
	@ApiModelProperty(required = true, value = "")
	public Integer getReplayCount() {
		return replayCount;
	}

	public void setReplayCount(Integer replayCount) {
		this.replayCount = replayCount;
	}

	public ErrorInfo lastError(String lastError) {
		this.lastError = lastError;
		return this;
	}

	/**
	 * Get lastError
	 * 
	 * @return lastError
	 **/
	@ApiModelProperty(required = true, value = "")
	public String getLastError() {
		return lastError;
	}

	public void setLastError(String lastError) {
		this.lastError = lastError;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ErrorInfo errorInfo = (ErrorInfo) o;
		return Objects.equals(this.replayCount, errorInfo.replayCount)
				&& Objects.equals(this.lastError, errorInfo.lastError);
	}

	@Override
	public int hashCode() {
		return Objects.hash(replayCount, lastError);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ErrorInfo {\n");

		sb.append("    replayCount: ").append(toIndentedString(replayCount)).append("\n");
		sb.append("    lastError: ").append(toIndentedString(lastError)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
