package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.time.OffsetDateTime;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * This is generic common header for any API method request or response message.
 */
@ApiModel(description = "This is generic common header for any API method request or response message.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommonHeader {
	@SerializedName("originalAPITransactionId")
	private String originalAPITransactionId = null;

	@SerializedName("sourceSystem")
	private String sourceSystem = null;

	@SerializedName("sourceUser")
	private String sourceUser = null;

	@SerializedName("timestamp")
	private OffsetDateTime timestamp = null;

	@SerializedName("correlationId")
	private String correlationId = null;

	@SerializedName("replayOnFailure")
	private Boolean replayOnFailure = false;

	@SerializedName("replayInterval")
	private ReplayInterval replayInterval = null;

	@SerializedName("replyAfter")
	private Double replyAfter = null;

	@SerializedName("replayThreshold")
	private Integer replayThreshold = 5;

	@SerializedName("originalMessage")
	private String originalMessage = null;

	@SerializedName("errorInfo")
	private ErrorInfo errorInfo = null;

	public CommonHeader originalAPITransactionId(String originalAPITransactionId) {
		this.originalAPITransactionId = originalAPITransactionId;
		return this;
	}

	/**
	 * Get originalAPITransactionId
	 * 
	 * @return originalAPITransactionId
	 **/
	@ApiModelProperty(required = true, value = "")
	public String getOriginalAPITransactionId() {
		return originalAPITransactionId;
	}

	public void setOriginalAPITransactionId(String originalAPITransactionId) {
		this.originalAPITransactionId = originalAPITransactionId;
	}

	public CommonHeader sourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
		return this;
	}

	/**
	 * Get sourceSystem
	 * 
	 * @return sourceSystem
	 **/
	@ApiModelProperty(required = true, value = "")
	public String getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	public CommonHeader sourceUser(String sourceUser) {
		this.sourceUser = sourceUser;
		return this;
	}

	/**
	 * Get sourceUser
	 * 
	 * @return sourceUser
	 **/
	@ApiModelProperty(required = true, value = "")
	public String getSourceUser() {
		return sourceUser;
	}

	public void setSourceUser(String sourceUser) {
		this.sourceUser = sourceUser;
	}

	public CommonHeader timestamp(OffsetDateTime timestamp) {
		this.timestamp = timestamp;
		return this;
	}

	/**
	 * Get timestamp
	 * 
	 * @return timestamp
	 **/
	@ApiModelProperty(required = true, value = "")
	public OffsetDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(OffsetDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public CommonHeader correlationId(String correlationId) {
		this.correlationId = correlationId;
		return this;
	}

	/**
	 * Get correlationId
	 * 
	 * @return correlationId
	 **/
	@ApiModelProperty(value = "")
	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public CommonHeader replayOnFailure(Boolean replayOnFailure) {
		this.replayOnFailure = replayOnFailure;
		return this;
	}

	/**
	 * Get replayOnFailure
	 * 
	 * @return replayOnFailure
	 **/
	@ApiModelProperty(example = "false", value = "")
	public Boolean isReplayOnFailure() {
		return replayOnFailure;
	}

	public void setReplayOnFailure(Boolean replayOnFailure) {
		this.replayOnFailure = replayOnFailure;
	}

	public CommonHeader replayInterval(ReplayInterval replayInterval) {
		this.replayInterval = replayInterval;
		return this;
	}

	/**
	 * Get replayInterval
	 * 
	 * @return replayInterval
	 **/
	@ApiModelProperty(required = true, value = "")
	public ReplayInterval getReplayInterval() {
		return replayInterval;
	}

	public void setReplayInterval(ReplayInterval replayInterval) {
		this.replayInterval = replayInterval;
	}

	public CommonHeader replyAfter(Double replyAfter) {
		this.replyAfter = replyAfter;
		return this;
	}

	/**
	 * Get replyAfter
	 * 
	 * @return replyAfter
	 **/
	@ApiModelProperty(value = "")
	public Double getReplyAfter() {
		return replyAfter;
	}

	public void setReplyAfter(Double replyAfter) {
		this.replyAfter = replyAfter;
	}

	public CommonHeader replayThreshold(Integer replayThreshold) {
		this.replayThreshold = replayThreshold;
		return this;
	}

	/**
	 * Get replayThreshold
	 * 
	 * @return replayThreshold
	 **/
	@ApiModelProperty(example = "5", value = "")
	public Integer getReplayThreshold() {
		return replayThreshold;
	}

	public void setReplayThreshold(Integer replayThreshold) {
		this.replayThreshold = replayThreshold;
	}

	public CommonHeader originalMessage(String originalMessage) {
		this.originalMessage = originalMessage;
		return this;
	}

	/**
	 * Get originalMessage
	 * 
	 * @return originalMessage
	 **/
	@ApiModelProperty(value = "")
	public String getOriginalMessage() {
		return originalMessage;
	}

	public void setOriginalMessage(String originalMessage) {
		this.originalMessage = originalMessage;
	}

	public CommonHeader errorInfo(ErrorInfo errorInfo) {
		this.errorInfo = errorInfo;
		return this;
	}

	/**
	 * Get errorInfo
	 * 
	 * @return errorInfo
	 **/
	@ApiModelProperty(value = "")
	public ErrorInfo getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(ErrorInfo errorInfo) {
		this.errorInfo = errorInfo;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		CommonHeader commonHeader = (CommonHeader) o;
		return Objects.equals(this.originalAPITransactionId, commonHeader.originalAPITransactionId)
				&& Objects.equals(this.sourceSystem, commonHeader.sourceSystem)
				&& Objects.equals(this.sourceUser, commonHeader.sourceUser)
				&& Objects.equals(this.timestamp, commonHeader.timestamp)
				&& Objects.equals(this.correlationId, commonHeader.correlationId)
				&& Objects.equals(this.replayOnFailure, commonHeader.replayOnFailure)
				&& Objects.equals(this.replayInterval, commonHeader.replayInterval)
				&& Objects.equals(this.replyAfter, commonHeader.replyAfter)
				&& Objects.equals(this.replayThreshold, commonHeader.replayThreshold)
				&& Objects.equals(this.originalMessage, commonHeader.originalMessage)
				&& Objects.equals(this.errorInfo, commonHeader.errorInfo);
	}

	@Override
	public int hashCode() {
		return Objects.hash(originalAPITransactionId, sourceSystem, sourceUser, timestamp, correlationId,
				replayOnFailure, replayInterval, replyAfter, replayThreshold, originalMessage, errorInfo);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class CommonHeader {\n");

		sb.append("    originalAPITransactionId: ").append(toIndentedString(originalAPITransactionId)).append("\n");
		sb.append("    sourceSystem: ").append(toIndentedString(sourceSystem)).append("\n");
		sb.append("    sourceUser: ").append(toIndentedString(sourceUser)).append("\n");
		sb.append("    timestamp: ").append(toIndentedString(timestamp)).append("\n");
		sb.append("    correlationId: ").append(toIndentedString(correlationId)).append("\n");
		sb.append("    replayOnFailure: ").append(toIndentedString(replayOnFailure)).append("\n");
		sb.append("    replayInterval: ").append(toIndentedString(replayInterval)).append("\n");
		sb.append("    replyAfter: ").append(toIndentedString(replyAfter)).append("\n");
		sb.append("    replayThreshold: ").append(toIndentedString(replayThreshold)).append("\n");
		sb.append("    originalMessage: ").append(toIndentedString(originalMessage)).append("\n");
		sb.append("    errorInfo: ").append(toIndentedString(errorInfo)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
