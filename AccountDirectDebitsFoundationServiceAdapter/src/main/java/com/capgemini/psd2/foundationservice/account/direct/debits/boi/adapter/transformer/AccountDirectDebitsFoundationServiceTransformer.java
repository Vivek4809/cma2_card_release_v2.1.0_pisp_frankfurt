
package com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.transformer;

import java.util.ArrayList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBReadDirectDebit1;
import com.capgemini.psd2.aisp.domain.OBReadDirectDebit1Data;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountDirectDebitsResponse;
import com.capgemini.psd2.aisp.transformer.AccountDirectDebitsTransformer;
import com.capgemini.psd2.validator.PSD2Validator;



@Component
public class AccountDirectDebitsFoundationServiceTransformer implements AccountDirectDebitsTransformer
{

	@Autowired
	private PSD2Validator validator;

	@Override
	public <T> PlatformAccountDirectDebitsResponse transformAccountDirectDebits(T source, Map<String, String> params) {
		PlatformAccountDirectDebitsResponse accountGETResponse1 = new PlatformAccountDirectDebitsResponse();
		OBReadDirectDebit1 data = new OBReadDirectDebit1();
		OBReadDirectDebit1Data oBReadDirectDebit1Data = new OBReadDirectDebit1Data();
		oBReadDirectDebit1Data.setDirectDebit(new ArrayList<>());
		data.setData(oBReadDirectDebit1Data);
		accountGETResponse1.setoBReadDirectDebit1(data);
		return accountGETResponse1;
	
	}

}
