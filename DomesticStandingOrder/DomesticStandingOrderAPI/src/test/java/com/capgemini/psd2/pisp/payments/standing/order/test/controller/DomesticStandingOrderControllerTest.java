package com.capgemini.psd2.pisp.payments.standing.order.test.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.DStandingOrderPOST201Response;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticStandingOrder1;
import com.capgemini.psd2.pisp.payments.standing.order.controller.DomesticStandingOrderController;
import com.capgemini.psd2.pisp.payments.standing.order.service.DomesticStandingOrderService;
import com.capgemini.psd2.pisp.payments.standing.order.test.mockdata.DomesticStandingOrderControllerMockData;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticStandingOrderControllerTest {

	private MockMvc mockMvc;

	@Mock
	private DomesticStandingOrderService service;

	@InjectMocks
	private DomesticStandingOrderController controller;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(controller).dispatchOptions(true).build();
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testCreateDomesticStandingOrderSubmissionResource() throws Exception {
		DStandingOrderPOST201Response response = new DStandingOrderPOST201Response();
		OBWriteDomesticStandingOrder1 domesticStandinOrderResponse = new OBWriteDomesticStandingOrder1();

		Mockito.when(service.createDomesticStandingOrderResource(any())).thenReturn(response);

		this.mockMvc
				.perform(post("/domestic-standing-orders").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
						.content(DomesticStandingOrderControllerMockData.asJsonString(domesticStandinOrderResponse)))
				.andExpect(status().isCreated());

		controller.createDomesticStandingOrderSubmissionResource(domesticStandinOrderResponse);
	}

	@Test(expected = PSD2Exception.class)
	public void testCreateDomesticStandingOrderSubmissionResource_InvalidRequest() {
		OBWriteDomesticStandingOrder1 domesticStandinOrderResponse = new OBWriteDomesticStandingOrder1();

		ReflectionTestUtils.setField(controller, "responseErrorMessageEnabled", "true");
		Mockito.when(service.createDomesticStandingOrderResource(any())).thenThrow(
				PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_DEBITOR_AGENT_IDENTIFICATION_INVALID));

		try {
			this.mockMvc
					.perform(
							post("/domestic-standing-orders").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
									.content(DomesticStandingOrderControllerMockData
											.asJsonString(domesticStandinOrderResponse)))
					.andExpect(status().isBadRequest());
		} catch (Exception e) {
			assertEquals(false, e.getMessage().contains("In Valid HTTP Request"));
		}
		controller.createDomesticStandingOrderSubmissionResource(domesticStandinOrderResponse);
	}

	@Test
	public void tesrRetrieveDomesticStandingOrderSubmissionResource() throws Exception {

		DStandingOrderPOST201Response response = new DStandingOrderPOST201Response();
		String consentId = "6369";

		Mockito.when(service.retrieveDomesticStandingOrderResource(consentId)).thenReturn(response);

		this.mockMvc
				.perform(get("/domestic-standing-orders/{DomesticStandingOrderId}", 6369)
						.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
						.content(DomesticStandingOrderControllerMockData.asJsonString(response)))
				.andExpect(status().isOk());

		controller.retrieveDomesticStandingOrderSubmissionResource(consentId);
	}

	@Test(expected = PSD2Exception.class)
	public void tesrRetrieveDomesticStandingOrderSubmissionResource_BadRequest() {

		String consentId = "6369";

		ReflectionTestUtils.setField(controller, "responseErrorMessageEnabled", "true");
		Mockito.when(service.retrieveDomesticStandingOrderResource(consentId)).thenThrow(
				PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_DEBITOR_AGENT_IDENTIFICATION_INVALID));

		try {
			this.mockMvc
					.perform(get("/domestic-standing-orders/{DomesticStandingOrderId}", 6369)
							.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
							.content(DomesticStandingOrderControllerMockData.asJsonString(any())))
					.andExpect(status().isBadRequest());
		} catch (Exception e) {
			assertEquals(false, e.getMessage().contains("In Valid HTTP Request/DomesticConsentId"));
		}

		controller.retrieveDomesticStandingOrderSubmissionResource(consentId);
	}
}
