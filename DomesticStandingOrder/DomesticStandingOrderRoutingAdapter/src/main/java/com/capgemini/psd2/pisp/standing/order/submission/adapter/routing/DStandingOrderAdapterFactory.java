package com.capgemini.psd2.pisp.standing.order.submission.adapter.routing;

import com.capgemini.psd2.pisp.adapter.DomesticStandingOrderAdapter;

public interface DStandingOrderAdapterFactory {
	
	public DomesticStandingOrderAdapter getDomesticStandingOrdersInstance(String coreSystemName);

}
