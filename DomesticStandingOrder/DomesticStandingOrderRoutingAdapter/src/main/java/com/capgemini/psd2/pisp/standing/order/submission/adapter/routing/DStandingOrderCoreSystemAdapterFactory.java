package com.capgemini.psd2.pisp.standing.order.submission.adapter.routing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.adapter.DomesticStandingOrderAdapter;

@Lazy
@Component
public class DStandingOrderCoreSystemAdapterFactory implements ApplicationContextAware, DStandingOrderAdapterFactory {

	/** The application context. */
	private ApplicationContext applicationContext;

	@Override
	public DomesticStandingOrderAdapter getDomesticStandingOrdersInstance(String coreSystemName) {
		return (DomesticStandingOrderAdapter) applicationContext.getBean(coreSystemName);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

}
