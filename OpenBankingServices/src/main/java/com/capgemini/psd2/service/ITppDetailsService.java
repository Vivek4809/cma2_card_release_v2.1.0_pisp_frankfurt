package com.capgemini.psd2.service;

import java.util.List;

import com.capgemini.psd2.model.OBThirdPartyProviders;

public interface ITppDetailsService {

	public OBThirdPartyProviders checkTppStatus(String tppid, String attributes);

	public boolean checkPassporting(OBThirdPartyProviders oBThirdPartyProviders, List<String> roles, String tenantid);
	
	public boolean passporting(String id, String roles, String tenantid, String flag);
	
	public OBThirdPartyProviders getPassportingList(String id, String flag); 

}
