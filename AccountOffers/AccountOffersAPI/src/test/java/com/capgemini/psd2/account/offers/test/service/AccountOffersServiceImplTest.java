package com.capgemini.psd2.account.offers.test.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.offers.service.impl.AccountOffersServiceImpl;
import com.capgemini.psd2.account.offers.test.mock.data.AccountOffersTestMockData;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountOffersAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.OBReadOffer1;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountOffersValidatorImpl;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;


@RunWith(SpringJUnit4ClassRunner.class)
public class AccountOffersServiceImplTest {

	@Mock
	private LoggerUtils loggerUtils;

	@Mock
	private AccountOffersAdapter adapter;

	@Mock
	private AispConsentAdapter aispConsentAdapter;

	@Mock
	private RequestHeaderAttributes headerAtttributes;

	@Mock
	AccountMappingAdapter accountMappingAdapter;

	@Mock
	private AccountOffersValidatorImpl accountsCustomValidator;

	@InjectMocks
	private AccountOffersServiceImpl service = new AccountOffersServiceImpl();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(headerAtttributes.getToken()).thenReturn(AccountOffersTestMockData.getToken());
		when(aispConsentAdapter.retrieveAccountMapping(anyString()))
				.thenReturn(AccountOffersTestMockData.getMockAccountMapping());

	}

	@Test
	public void retrieveAccountOffersSuccessWithLinkMeta() {
		accountsCustomValidator.validateUniqueId(anyObject());
		
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountOffersTestMockData.getMockAispConsent());

		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountOffersTestMockData.getMockAccountMapping());

		Mockito.when(adapter.retrieveAccountOffers(anyObject(), anyObject()))
				.thenReturn(AccountOffersTestMockData.getAccountOffersMockPlatformResponseWithLinksMeta());

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountOffersTestMockData.getMockLoggerData());

		accountsCustomValidator.validateResponseParams(anyObject());

		OBReadOffer1 actualResponse = service.retrieveAccountOffers("d9d9c3dc-1427-4f03-92a7-7916fcf4cc0d");

		assertEquals(AccountOffersTestMockData.getMockExpectedAccountOffersWithLinksMeta(), actualResponse);
	}

	@Test
	public void retrieveAccountOfferSuccessWithoutLinksMeta() {
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountOffersTestMockData.getMockAispConsent());

		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountOffersTestMockData.getMockAccountMapping());

		Mockito.when(adapter.retrieveAccountOffers(anyObject(), anyObject()))
				.thenReturn(AccountOffersTestMockData.getAccountOffersMockPlatformResponseWithoutLinksMeta());

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountOffersTestMockData.getMockLoggerData());

		accountsCustomValidator.validateResponseParams(anyObject());

		OBReadOffer1 actualResponse = service.retrieveAccountOffers("123");

		assertEquals(AccountOffersTestMockData.getMockExpectedAccountOffersWithoutLinksMeta(), actualResponse);
	}

	@After
	public void tearDown() throws Exception {
		service = null;
	}

}
